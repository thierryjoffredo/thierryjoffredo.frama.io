baseURL = 'https://thierryjoffredo.frama.io/'
languageCode = 'fr-fr'
title = 'Miscellanées numériques'
theme = 'typo'

[taxonomies]
tag = 'tags'

[params]
# Appearance settings
theme = 'auto'

# Intro on main page, content in markdown
# homeIntroTitle = 'Bienvenue !'
homeIntroContent = """
![Newton, statue d'Eduardo Paolozzi (1995) installée à l'entrée de la British Library à Londres](/images/newton.png#small)

Bienvenue ! Je m'appelle Thierry Joffredo. Prof de maths de formation, je suis engagé depuis 2008 dans le service public du numérique pour l'éducation, et travaille au sein de la DSII de l'académie de Rennes. Promoteur de l’utilisation de ressources et de logiciels libres dans l’éducation, je suis également très attentif aux questions des libertés numériques, de la protection de la vie privée et de l’inclusion, soucieux d’une éducation au numérique qui s’adresse à tous les membres de la communauté éducative, plus particulièrement celles et ceux qui en sont le plus éloigné⋅es. Je tâche de recenser et partager quelques réflexions ou découvertes dans les rubriques de ce site ([blog](posts), [biblio](a-lire), [audio](a-ecouter), [video](a-voir)) et alimente une veille sur [une collection Flus](https://app.flus.fr/collections/1732141437309404145).

Pour soutenir des associations qui agissent pour nos libertés numériques, c'est [par ici](soutenir). 

Je suis par ailleurs chercheur en histoire des mathématiques, auteur d'une [thèse de doctorat](https://tel.archives-ouvertes.fr/tel-01852029v2), soutenue en décembre 2017, sur un traité des courbes algébriques publié en 1750. Je suis particulièrement engagé dans le projet d’humanités numériques [_ENCCRE_](http://enccre.academie-sciences.fr) (Édition Numérique Collaborative et CRitique de l’_Encyclopédie_ de Diderot et D'Alembert), chargé de la valorisation du projet, notamment auprès des publics scolaires. Vous pouvez consulter mon CV de recherche (publications, communications) [ici, sur HAL](https://cv.archives-ouvertes.fr/thierry-joffredo) ainsi que mon carnet de recherche [Courbes et osculations](https://cramer.hypotheses.org/) sur la plateforme hypotheses.org.

Les contenus de ce site sont mis à disposition sous licence [Creative Commons BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr). Illustration : [_Newton after Blake_, statue d'Edward Paolozzi](https://en.wikipedia.org/wiki/Newton_(Paolozzi)), installée à l'entrée de la British Library à Londres (photo personnelle)
"""

# Collection to display on home
# homeCollectionTitle = 'Posts'
# homeCollection = 'posts'

# Lists parameters
paginationSize = 100
listSummaries = true
listDateFormat = '2 Jan 2006'

# Breadcrumbs
breadcrumbs = false

# Social icons

[[params.social]]
name = "mastodon"
url = "https://www.mamot.fr/@ThierryJoffredo"

[[params.social]]
name = "gitlab"
url = "https://framagit.org/thierryjoffredo"

[[params.social]]
name = "orcid"
url = "https://orcid.org/0000-0002-2689-4204"


[[params.social]]
name = "email"
url = "mailto:thierry@joffredolebrun.fr"

[[params.social]]
name = "rss"
url = "https://thierryjoffredo.frama.io/posts/index.xml"


# Main menu pages
[[params.menu]]
name = "accueil"
url = "/"

[[params.menu]]
name = "blog"
url = "/posts"

[[params.menu]]
name = "biblio"
url = "/a-lire"

[[params.menu]]
name = "audio"
url = "/a-ecouter"

[[params.menu]]
name = "vidéo"
url = "/a-voir"

[[params.menu]]
name = "veille"
url = "https://app.flus.fr/collections/1732141437309404145"

# [[params.menu]]
# name = "tags"
# url = "/tags"
