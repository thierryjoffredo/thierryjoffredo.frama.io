---
title: 'À propos'
---

**Numérique pour l'éducation**

Enseignant en mathématiques de formation, je suis engagé depuis 2008 dans le développement du service public du numérique pour l'éducation. Je suis aujourd'hui responsable adjoint du pôle Applications et utilisateurs à la DSII de l'académie de Rennes, au sein duquel nous travaillons à développer et accompagner l'offre de services numériques en ligne aux écoles, établissements et services académiques au sein de l'environnement numérique académique [Toutatice](https://www.toutatice.fr). 

Promoteur de l’utilisation de ressources et de logiciels libres dans l’éducation, je suis également très attentif aux questions des libertés numériques, de la protection de la vie privée et de l’inclusion, soucieux d’une éducation au numérique qui s’adresse à tous les membres de la communauté éducative, plus particulièrement celles et ceux qui en sont le plus éloigné⋅es.

**Histoire des mathématiques**

Je suis par ailleurs chercheur en histoire des mathématiques, auteur d'une [thèse de doctorat](https://tel.archives-ouvertes.fr/tel-01852029v2), soutenue en décembre 2017, sur un traité des courbes algébriques du savant genevois Gabriel Cramer publié en 1750. Je suis membre associé de deux unités mixtes de recherche : les [Archives Henri-Poincaré à Nancy](https://poincare.univ-lorraine.fr/fr/membre-associe/thierry-joffredo) et l’équipe Histoire des sciences mathématiques de l'[Institut de mathématiques de Jussieu - Paris Rive Gauche](https://webusers.imj-prg.fr/~thierry.joffredo/). 

Je suis très engagé dans le projet d’humanités numériques [_ENCCRE_](http://enccre.academie-sciences.fr) (Édition Numérique Collaborative et CRitique de l’_Encyclopédie_ de Diderot et D'Alembert), membre du comité de lecture et chargé de la valorisation du projet, notamment auprès des publics scolaires. Je participe également à deux projets de recherche financés par l'ANR pour 2022-2025 : "[Patrimaths - Patrimoine et patrimonialisation des mathématiques, 18e-20e siècles](https://patrimaths.hypotheses.org)" et "[VHS - Vision artificielle et analyse historique de la circulation de l'illustration scientifique](https://vhs.hypotheses.org/)", ainsi qu'au projet "Brouillons mathématiques. Critique génétique et fabrication des savoirs en mathématique", lauréat 2021 de l'appel à projets Émergence(s) de la Ville de Paris (voir le [programme du séminaire 2022 à l'ITEM](http://www.item.ens.fr/sem-manuscrits-scientifiques/)).

Vous pouvez consulter mon CV de recherche (publications, communications) [ici, sur HAL](https://cv.archives-ouvertes.fr/thierry-joffredo) ainsi que mon carnet de recherche [Courbes et osculations](https://cramer.hypotheses.org/) sur la plateforme hypotheses.org.
