---
title: 'Quelques podcasts incontournables sur le numérique et la tech.'
toc: true
---

## Le code a changé, Xavier de La Porte, France Inter

Pour aborder les grands enjeux du numérique au travers des étonnements et des questionnements très personnels de l’auteur du podcast, avec l’éclairage de ses invités.

Épisodes et abonnement : <https://www.franceinter.fr/emissions/le-code-a-change>

Quelques épisodes choisis :

- [Le pylône qui valait 5 millions de dollars](https://www.franceinter.fr/emissions/le-code-a-change/le-pylone-qui-valait-5-millions-de-dollars), avec Alexandre Laumonier
- [Ce que les pouvoirs gagnent à tout savoir de nos vies](https://www.franceinter.fr/emissions/le-code-a-change/ce-que-les-pouvoirs-gagnent-a-tout-savoir-de-nos-vies), avec Bernard Harcourt
- [Archiver le web : une entreprise folle et merveilleuse](https://www.franceinter.fr/emissions/le-code-a-change/archiver-le-web-une-entreprise-folle-et-merveilleuse-avec-valerie-schafer), avec Valérie Schafer
- [Comment la traduction automatique s’est-elle mise à (mieux) marcher ?](https://www.franceinter.fr/emissions/le-code-a-change/la-traduction-automatique-avec-thierry-poibeau) avec Thierry Poibeau
- [COVID, confinement et grande conversion numérique](https://www.franceinter.fr/emissions/le-code-a-change/covid-confinement-et-grande-conversion-numerique-avec-antonio-casilli), avec Antonio Casilli
- [Pourquoi s’est-on mis à tout noter ?](https://www.franceinter.fr/emissions/le-code-a-change/pourquoi-s-est-on-mis-tout-noter-avec-vincent-coquaz) avec Vincent Coquaz
- [Ils cherchent “les trucs bizarres qu’il y a dans vos téléphones” : rencontre avec des traqueurs de trackers](https://www.franceinter.fr/emissions/le-code-a-change/ils-cherchent-les-trucs-bizarres-qu-il-y-a-dans-vos-telephones-rencontre-avec-des-traqueurs-de-trackers), avec Exodus Privacy
- [Coincé dans Zoom](https://www.franceinter.fr/emissions/le-code-a-change/coince-dans-zoom-avec-hubert-guillaud), avec Hubert Guillaud
- [L’homme qui avait la formule mathématique des bonnes histoires](https://www.franceinter.fr/emissions/le-code-a-change/l-homme-qui-avait-la-formule-mathematique-des-bonnes-histoires), avec Yves Bergquist
- [Quand l'État a voulu devenir une plateforme](https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/le-code-a-change-9-3818194), avec Marie Alauzen

## Libre à vous, Cause commune

Une émission hebdomadaire animée par l’April, pour comprendre les enjeux du logiciel libre dans notre vie numérique quotidienne, dans l’administration, l’éducation, la santé, etc.

Épisodes et abonnement : <https://cause-commune.fm/shows/libre-a-vous/>

Quelques épisodes choisis :

- [\#63 – Musique libre – Visioconférences – École – Cryptpad – Confinement](https://cause-commune.fm/podcast/63-musique-libre-visioconferences-ecole-crytpad-confinement/), avril 2020
- [\#65 – Logiciels libres dans l’Éducation nationale – Oisux – Easter-Eggs](https://cause-commune.fm/podcast/65-logiciels-libres-dans-leducation-nationale-oisux-easter-eggs/), mai 2020
- [\#88 – Henri Verdier (Ambassadeur pour le numérique) – ARCAF – Collectivités et logiciel libre](https://cause-commune.fm/podcast/libre-a-vous-88/), janvier 2021
- [\#94 Démarches pro-libre dans l’éducation>](https://cause-commune.fm/podcast/63-musique-libre-visioconferences-ecole-crytpad-confinement/) Démarches pro-libre dans l’éducation, février 2021
- \#150 – [Échange avec Alexis Kauffmann sur le libre éducatif – La Fête des possibles – Le libre et la sobriété énergétique](https://www.libreavous.org/150-echange-avec-alexis-kauffmann-sur-le-libre-educatif-la-fete-des-possibles), Septembre 2022

## Le Meilleur des mondes – France Culture

Quelques épisodes choisis :

- [Slow Tech : pourquoi il est urgent de ralentir](https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/slow-tech-pourquoi-il-est-urgent-de-ralentir-8068285), avril 2022
- [Les Civic Tech peuvent-elles nous réconcilier avec la politique ?](https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/les-civic-tech-peuvent-elles-nous-reconcilier-avec-la-politique-5456416) avril 2022
- [Web 3.0 : Internet peut-il redevenir un bien commun ?](https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/le-meilleur-des-mondes-du-vendredi-29-avril-2022-5686300) avril 2022
- [Safe cities : les enjeux d’une politique de surveillance](https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/safe-cities-les-enjeux-d-une-politique-de-surveillance-4747030), juin 2022

## La Cantine numérique de Brest – An Daol Vras

Entrevues et rencontres avec des acteurs et actrices du numérique.

Episodes : <https://www.lacantine-brest.net/category/podcast/>  
Abonnement (RSS) : <https://www.lacantine-brest.net/feed/podcast/>


## LSD La série documentaire, France Culture

Excellente série d’Antoine Tricot, intitulée « À l’ère de la surveillance numérique », diffusée en avril 2021 (4 épisodes) :

<https://www.franceculture.fr/emissions/series/a-lere-de-la-surveillance-numerique>

## Une histoire de… l’Internet

Huit épisodes de 14 minutes chacun pour « remonter aux origines de ce réseau des réseaux, où l’on croisera des individus épris d’utopie et de liberté aux Etats-Unis, mais aussi des ingénieurs entêtés en France face aux poids des télécoms, des chercheurs en ex-URSS appelés à échanger avec le monde libre, ou encore, en Chine… des fonctionnaires obsédés par le contrôle de l’information. ». Podcast signé Julien Le Bot pour France Culture, diffusé en février 2022

<https://www.franceculture.fr/emissions/serie/une-histoire-de-l-internet>

## Mécaniques de la cybermenace

« Dans ce podcast, France Culture vous emmène dans des lieux fermés au public à la rencontre d’acteurs qui imposent souvent une part d’anonymat et de secret, pour comprendre comment la France se défend et s’arme dans le cyberespace. » Cinq épisodes de 10 minutes environ pour ce podast de France Culture, diffusé en mars 2022.

<https://www.franceculture.fr/emissions/mecaniques-de-la-cybermenace>

## Aux sources du numérique

Chaque mois, un·e auteur·e qui éclaire la société numérique. Aux sources du numérique, le book club de la société numérique. Des rencontres organisées au Tank (Paris) par l’agence [Spintank](https://www.spintank.fr/) et le think tank [Renaissance Numérique](https://www.renaissancenumerique.org/).

Épisodes et abonnement : <https://podcast.ausha.co/aux-sources-du-numerique-asdn>

Épisodes choisis :

- Internet sait-il déjà tout de nous ? avec Olivier Tesquet : <https://podcast.ausha.co/aux-sources-du-numerique-asdn/internet-sait-il-deja-tout-de-nous-olivier-tesquet-avril-2020>
- Démocratie et Internet sont-ils irréconciliables ? avec Félix Tréguer : <https://podcast.ausha.co/aux-sources-du-numerique-asdn/democratie-et-internet-sont-ils-irreconciliables-felix-treguer-fevrier-20>

