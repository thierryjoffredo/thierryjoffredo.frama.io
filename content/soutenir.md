---
title: 'Soutenir'
---

- Soutenir Framasoft, pour contribuer à une société empreinte de justice sociale où le numérique permet aux humain·es de s’émanciper, à contre-courant des imaginaires du capitalisme de surveillance : [https://soutenir.framasoft.org/fr/](https://soutenir.framasoft.org/fr/)
[![Logo de Framasoft](/mediatheque/Soutenir-Framasoft.png#center)](https://soutenir.framasoft.org/fr/)  

- Adhérer à l'APRIL, principale association de promotion et de défense du logiciel libre dans l’espace francophone : [https://www.april.org/adherer](https://www.april.org/adherer)
[![Logo de l'APRIL](/mediatheque/banniere_horizontale_soutien_adherent_fulltext_300x100.png#center)](https://www.april.org/adherer)  

- Soutenir la Quadrature du Net, qui promeut et défend les libertés fondamentales dans l’environnement numérique, lutte contre la censure et la surveillance, questionne la façon dont le numérique et la société s’influencent mutuellement, œuvre pour un Internet libre, décentralisé et émancipateur : [https://don.laquadrature.net/](https://don.laquadrature.net/)
[![Logo de la Quadrature du Net](/mediatheque/quadrature.jpeg#center)](https://don.laquadrature.net/)  

