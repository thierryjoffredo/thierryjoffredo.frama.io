---
title: 'Ma petite bibliothèque'
---


### *Technologies partout, démocratie nulle part*, Irénée Régnauld et Yaël Benayoun, FYP éditions, 2020.  
<https://www.fypeditions.com/technologies-partout-democratie-nulle-part/>  
Une passionnante revue des grands défis actuels posés par le numérique et la technologique, à la lumière de leurs enjeux démocratiques. [Note de lecture](/posts/2020-09-28-note-de-lecture-technologies-partout-democratie-nulle-part-de-yael-benayoun-et-irenee-regnauld/).

### *L’utopie déchue, une contre-histoire d’Internet XVe-XXIe siècle*, Félix Tréguer, Fayard, 2019  
<https://www.fayard.fr/sciences-humaines/lutopie-dechue-9782213710044>  
Une histoire longue du contrôle de l’espace public, de l’invention de l’imprimerie jusqu’à aujourd’hui. Un ouvrage dense et fort bien documenté (issu d’une [thèse soutenue à l’EHESS en 2017](http://theses.fr/2017EHES0117)) qui éclaire l’histoire récente d’Internet, à la fois vu comme outil de domination et levier d’émancipation ([Note de lecture](/posts/2020-12-29-lutopie-dechue-une-contre-histoire-dinternet-xve-xxie-siecle-de-felix-treguer/)).

### *Culture numérique*, Dominique Cardon, Presses de Sciences Po, 2019  
<http://www.pressesdesciencespo.fr/fr/book/?GCOI=27246100540390>  
De la nécessité de se forger une culture numérique (savoirs et pratiques) pour s’armer contre les défis de notre époque.

### *Surveillance://. Les libertés au défi du numérique : comprendre et agir*, Tristan Nitot, 2016, C &amp; F éditions  
<https://cfeditions.com/surveillance/>  
Un véritable manuel d’auto-défense numérique, pour apprendre à préserver ses espaces de liberté.

### *Déclic*, Anne-Sophie Jacques et Maxime Guedj, 2020, Les Arènes  
<https://declic-lelivre.com/>  
Très proche du précédent, dans son ambition d’accompagner le lecteur ou la lectrice dans les possibilités d’actions pour protéger sa vie privée sur Internet.

### *L’âge du capitalisme de surveillance,* Shoshanna Zuboff, éditions Zulma, 2020   
<https://www.zulma.fr/livre-lage-du-capitalisme-de-surveillance-572196.html>

[à suivre !]

