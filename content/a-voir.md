---
title: 'Quelques documentaires et vidéos à regarder et partager'
toc: true
---

## *Invisibles. Les travailleurs du clic*, France Télévision, avec Antonio Casilli (4 épisodes de 20 minutes environ)  
[https://www.france.tv/slash/invisibles/](https://www.france.tv/slash/invisibles/)
{{< peertube peertube.datagueule.tv f6usw2oj2jrgBbpWy8DxDx >}}

## *Nothing to hide*  
Rien à cacher, vraiment ? À propos de la surveillance généralisée et permise par nos propres usages numériques  
{{< peertube videos.domainepublic.net eaee7866-d209-4e5c-b7b0-443395b79c82 >}}

## *The Internet’s own boy – The story of Aaron Swartz*  
Le parcours tragique d’un petit génie de l’informatique, devenu défenseur des libertés numériques et militant pour un internet ouvert et émancipateur.  
{{< peertube videos.domainepublic.net dcafd5ca-acc2-456d-80e1-35626d3f40b0 >}}

## *LoL. Logiciel libre, une affaire sérieuse*, de François Zaïdi
Le logiciel est la base de l’informatique. Un téléphone portable, un réfrigérateur ou une voiture sont devenus des objets largement informatisés. Ces logiciels sont composés de code appelé code source. Or ce code a fini par être caché par les fabricants, car source de profits. À l’heure où les cinq plus grosses capitalisations boursières mondiales sont Google, Apple, Facebook, Amazon et Microsoft, les populations sont-elles au fait de l’impact que leur usage du numérique a sur leur vie ? 
{{< peertube videos.domainepublic.net cd3e9b26-0ad8-4437-b8d3-0b7b80136824 >}}



