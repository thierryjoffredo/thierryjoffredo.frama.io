---
id: 1130
title: 'Plaidoyer pour le RSS'
date: '2020-12-31T07:08:14+01:00'
author: thierry
layout: post
guid: 'https://www.miscellanea-numerica.fr/?p=1130'
permalink: /2020/12/31/plaidoyer-pour-le-rss/
image: /wp-content/uploads/2020/12/28188286432_3353c1c7ed_w.jpg
categories:
    - 'Libertés numériques'
---

Les flux [RSS (Real Simple Syndication)](https://fr.wikipedia.org/wiki/RSS) sont des ressources produites automatiquement par certains sites Web et qui permettent à des abonné⋅es de suivre l’actualité de ces sites, au travers de services appelés « agrégateurs », comme Feedly ou [TinyTinyRSS](https://tt-rss.org/) que j’utilise, par exemple. Généralement, un flux RSS contient le titre et un résumé de l’article, et un lien vers le site concerné. Voici l’url du flux RSS des actualités du site Next Inpact, par exemple : <https://www.nextinpact.com/rss/noih.xml>, celle de ce blog est <https://www.miscellanea-numerica.fr/feed/>. Sur un site, on repère la plupart du temps ce flux grâce à l’icône (source [Wikimedia commons](https://commons.wikimedia.org/wiki/File:Rss-feed.svg)) :

{{< figure src="/wp-content/uploads/2020/12/64px-Rss-feed.svg_.png" >}}

J’enregistre ces différentes url dans mon agrégateur pour réunir les flux d’actualités des sites auxquels je m’intéresse : je n’ai donc pas besoin de me rendre sur ces sites pour suivre leur actualité, mon agrégateur me permet de balayer d’un seul regard l’ensemble de leurs dernières mises à jour (que je peux ranger par catégories : j’ai environ 180 flux enregistrés sur mon agrégateur) et de cliquer sur le lien pour me rendre sur le site concerné lorsque je l’estime nécessaire :

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-31-1787-Tiny-Tiny-RSS-1024x644.png" >}}

On peut également, grâce à ces flux RSS, afficher sur un site les dernières actualités en provenance d’autres sites. C’est évidemment très pratique, voire indispensable, pour mener une veille sur des sujets qui nous intéressent.

Hélas – ce n’est pas nouveau, mais la tendance est à la hausse en ce moment – de plus en plus de sites abandonnent cette technologie et ne permettent plus de suivre leur actualité de cette manière. C’est particulièrement ennuyeux lorsqu’il s’agit de sites institutionnels importants qui n’intègrent plus cette possibilité pour les internautes, se contentant de proposer un abonnement à une newsletter ou de renvoyer à leurs réseaux sociaux (où les nouvelles publications sont annoncées). Cette dernière façon de faire a bien entendu un défaut majeur : cela oblige souvent à avoir soi-même un compte Twitter ou Facebook pour suivre cette actualité, et donc d’être pisté par ces plateformes (sans parler du risque de noyer l’information dans les timelines de ces réseaux, ou même de l’en voir écartée par les algorithmes de personnalisation de ces timelines), alors que le RSS est une technologie simple, efficace et non invasive pour la vie privée.

Voici quelques sites qui, sauf erreur, ont récemment arrêté de proposer un flux RSS à leurs lecteurs, souvent à l’occasion d’une refonte :

- Éducation : [education.gouv.fr](https://www.education.gouv.fr), [eduscol.education.fr](https://eduscol.education.fr/)
- Maths : <https://www.insmi.cnrs.fr/>, <https://culturemath.ens.fr/>
- Service public / État : <https://www.elysee.fr/>, <https://www.service-public.fr/>, <https://www.assemblee-nationale.fr/>, <https://www.cnil.fr/> (\[edit\] : flux RSS retrouvé depuis, il était bien caché : <https://www.cnil.fr/fr/rss.xml>)
- etc.

Je tâche de contacter, souvent sans succès, les équipes qui animent ces sites web pour leur demander de réintégrer un flux RSS pour être en mesure de suivre leur actualité : ce n’est pas techniquement très coûteux à mettre en place, et les services rendus à l’utilisateur sont grands, mais leur stratégie semble être très orientée vers les réseaux sociaux aujourd’hui (ça amène des abonné⋅es et des clics, quantifiables et valorisables) , ce qui ne va pas sans poser de questions – surtout pour des sites publics ! N’hésitez pas vous non plus à faire connaître votre mécontentement lorsqu’un tel site abandonne cette fonctionnalité.

À lire sur le même sujet, sur le Framablog : <https://framablog.org/2018/07/16/les-flux-rss-maintenant/>
