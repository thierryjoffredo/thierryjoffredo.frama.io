---
title: 'S’auto-former à la sécurité numérique'
date: '2021-08-01T11:18:28+02:00'
author: thierry
layout: post
permalink: /2021/08/01/sauto-former-a-la-securite-numerique/
categories:
    - 'Sécurité numérique'
---

Il n’est pas toujours simple de comprendre les cadres et de disposer des bonnes informations pour agir de manière éclairée en matière de protection des données et de sécurité numérique, que ce soit sur le plan des usages privés ou professionnels. Il n’est pas non plus évident de se dégager du temps pour le faire, mais au prix de quelques heures de disponibilité des ressources intéressantes sont à la disposition du grand public (mais je pense en premier lieu, bien entendu, à mes collègues agents des DSI des académies ou aux formateurs⋅rices numériques dans les DANE) pour travailler ces questions en autonomie.

## L’atelier RGPD de la CNIL

La Commission Nationale Informatique et Libertés (CNIL) propose depuis mars 2019 un cours ouvert en ligne intitulé « Atelier RGPD » : <https://atelier-rgpd.cnil.fr/> (voir l’annonce de lancement sur le site de la CNIL : <https://www.cnil.fr/fr/la-cnil-lance-sa-formation-en-ligne-sur-le-rgpd-ouverte-tous>). Ce cours est divisé en quatre modules : le RGPD et ses notions-clés, les principes de la protection des données, les responsabilités des acteurs, et le DPD et les outils de la conformité. L’attestation de réussite vous sera délivrée à partir du moment où vous aurez parcouru les quatre modules en répondant de manière correcte à au moins 80% des questions qui vous seront posées lors des évaluations. J’ai mis un peu plus de quatre heures pour le terminer, il y a deux ans. Le cours est aujourd’hui en rénovation (à la date où j’écris ce billet) et devrait être à nouveau accessible en septembre, mais peut-être pas gratuitement cette fois (en tous cas, la précédente mouture était déclarée comme gratuite jusqu’en septembre 2021).

{{< figure src="/wp-content/uploads/2021/08/mooc_atelier_rgpd.jpg" caption="Source de l’image : [site CNIL](https://www.cnil.fr/fr/la-cnil-lance-sa-formation-en-ligne-sur-le-rgpd-ouverte-tous)" >}}

## Le cours SecNumAcadémie de l’ANSSI

Dans des modalités assez proches, l’Agence nationale de la sécurité des systèmes d’information ([ANSSI](https://www.ssi.gouv.fr/)) propose également un cours ouvert en ligne intitulé SecNum*Académie*, accessible à l’adresse <https://secnumacademie.gouv.fr/>. Ce cours demande un peu plus de temps que l’atelier RGPD de la CNIL : j’ai compté un peu plus de sept heures pour parcourir les quatre modules et répondre aux évaluations. Il permet de s’initier aux questions et enjeux de la cybersécurité dans le premier module (panorama de la SSI), puis d’approfondir les sujets de la sécurité de l’authentification, de la navigation sur internet et du poste de travail (PC, terminaux mobiles) dans les trois modules suivants. Outre la consolidation et l’approfondissement des connaissances personnelles sur la sécurité numérique, ce cours donne des pistes de mise en œuvre d’actions pour se protéger et protéger les autres (l’expression est à la mode en ces temps de crise sanitaire) lorsque l’on utilise des outils ou des services numériques.

{{< figure src="/wp-content/uploads/2021/08/Screenshot-2021-08-01-at-08-36-01-Contenu-SecNumacademie-edited.png" caption="Source : capture d’écran personnelle [](https://secnumacademie.gouv.fr/content)" >}}

Dans les deux cas, l’approche est rigoureuse et « pédagogique », c’est-à-dire que les contenus (vidéos sous-titrées, textes, schémas, liens…) ont été bien pensés et mis en musique dans ces cours en ligne, avec des évaluations par QCM (avec plusieurs essais possibles) relativement simples, qui permettent à une personne ayant motivation et disponibilité de les suivre sans difficulté jusqu’au bout, indépendamment de sa culture numérique de départ. C’est donc une première entrée qui permet de se sensibiliser à ces questions : ensuite, pas de miracle, rien de tel que les cas d’usages en situation professionnelle pour mettre ses connaissances à l’épreuve du réel.

Il reste aussi à travailler sur les bonnes manières de transmettre ces connaissances et « bonnes pratiques » (savoirs-faire) et de les rendre facilement accessibles à son public-cible (formateurs⋅rices, collègues agents ou enseignants, élèves). Il y a plusieurs défis à relever dans l’Éducation nationale : faire prendre conscience des enjeux sans tomber dans l' »alarmisme » ni décourager, rendre autonomes les collègues sur les premiers réflexes à avoir lorsque l’on s’interroge sur la protection des données ou la sécurité de son poste de travail[^1], accompagner des collègues et des élèves sur l’utilisation d’un gestionnaire de mot de passe[^2], par exemple, n’ont rien d’évident et nécessitent d’engager des moyens importants pour communiquer, sensibiliser, former, apprendre. Nous pourrons nous appuyer, dans l’académie de Rennes, sur un réseau naissant de référents à la sécurité numérique dans les collèges et lycées, mais il y a encore beaucoup de chemin à parcourir pour toucher les collègues des services académiques ou des écoles, par exemple, dans un contexte de sur-sollicitation sur les sujets du numérique.

La protection des données et la sécurité font partie de la culture numérique que chacun⋅e doit acquérir pour être en maîtrise de ses usages, exercer et consolider ses libertés numériques, s’émanciper des acteurs qui marchandisent nos données, et participer à l’acculturation de ses proches dans et en dehors des cercles professionnels (familles, amis…) pour le bénéfice de tou⋅te⋅s. À ce titre, on peut aussi conseiller de se tourner vers Pix (<https://pix.fr>) pour s’entraîner sur ses compétences numériques, notamment celles liées à la sécurité numérique, qui traversent chacun des [cinq domaines de compétences](https://pix.fr/competences/), mais fait particulièrement l’objet du quatrième domaine, « Protection et sécurité ».

PS : on peut aussi jeter un œil à la plateforme du projet Totem <https://totem-project.org/fr/ > qui se propose de former journalistes et activistes à la sécurité numérique, il y a là beaucoup de contenus non spécifiques au public-cible qui peuvent donc intéresser toutes celles et ceux qui s’engagent pour s’informer et se former à la sécurité numérique.

## Pour aller plus loin

Ces différentes ressources permettent une bonne entrée en matière sur le sujet de la sécurité numérique, mais peuvent ne pas suffire pour des publics plus aguerris ou experts (les informaticien⋅nes, notamment). Les ressources suivantes restent accessibles mais permettent d’approfondir un peu plus les connaissances :

- le MOOC RGPD du CNAM : <https://cnamcil.wordpress.com/mooc/>
- la mallette Cyberedu de l’ANSSI : <https://www.ssi.gouv.fr/entreprise/formations/secnumedu/contenu-pedagogique-cyberedu/>
- le MOOC Protection de la vie privée dans le monde numérique de l’INRIA : <https://www.fun-mooc.fr/fr/cours/protection-de-la-vie-privee-dans-le-monde-numerique/>

Merci à la personne (qui se reconnaîtra) qui m’a signalé par mail ces ressources complémentaires.

[^1]: lorsqu’on en a un fourni par l’employeur, ce qui n’est pas le cas, loin de là, de tous les personnels, notamment les enseignant⋅es. 
[^2]: comme cela sera le cas en 2021-2022 dans l’académie de Rennes avec l’application CozyPass fournie avec l’espace numérique personnel MyToutatice.
