---
title: 'Joyeux anniversaire Wikipedia !'
date: '2021-01-15T14:02:29+01:00'
author: thierry
layout: post
image: /wp-content/uploads/2021/01/langfr-800px-Wikipedia-logo-v2-fr.svg_.png
categories:
    - 'Libertés numériques'
---

Aujourd’hui 15 janvier [Wikipedia](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia) fête ses [20 ans](https://www.wikimedia.fr/wikipedia-fete-ses-20-ans/) !

Inutile de présenter la célèbre encyclopédie en ligne, ce ne sont pas les [articles](https://www.lemonde.fr/pixels/article/2021/01/14/wikipedia-fete-les-20-ans-d-une-belle-et-exigeante-utopie_6066271_4408996.html), les [livres](https://www.placedeslibraires.fr/livre/9782412064207-wikipedia-au-coeur-de-la-plus-grande-encyclopedie-du-monde-remi-mathis/) ou les documentaires qui manquent en ce moment, comme par exemple cette vidéo diffusée sur Arte et [disponible sur arte.tv](https://www.arte.tv/fr/videos/093704-000-A/il-etait-une-fois-wikipedia/) jusqu’au 4 avril 2021 (mais que vous pourrez trouver assez facilement sur Peertube ensuite, grâce au moteur de recherche [Sepia](https://sepiasearch.org/) par exemple)[.](https://www.arte.tv/fr/videos/093704-000-A/il-etait-une-fois-wikipedia/)

{{< figure src="/wp-content/uploads/2021/01/Screenshot_2021-01-15-Wikipedia-HomePage-1024x649.png" caption="Page d’accueil de Wikipedia.org telle qu’elle a été enregistrée par la [WayBack Machine en juillet 2001](https://web.archive.org/web/20010727112808/http://www.wikipedia.org:80/)" >}}

En tant que contributeur du projet [ENCCRE](http://enccre.academie-sciences.fr/encyclopedie/) – Édition numérique collaborative et critique de l’Encyclopédie de Diderot, D’Alembert et Jaucourt, qui va, elle, sur ses 270 ans – les différences et proximités entre les deux entreprises encyclopédiques m’intéressent particulièrement. Je viens par exemple de lire cette chronique de Benoît Melançon, intitulée « Les vertus utopiques de l’encylopédisme » publiée (sous licence Creative Commons BY-NC-SA 4.0) sur son site [sens-public.org](http://www.sens-public.org/articles/1323/), que je vous invite à lire, et qui se conclut ainsi :

> Il est pourtant une caractéristique qui unit profondément les deux entreprises. L’une et l’autre sont des utopies, au sens positif du terme. Elles se servent de la technique réputée la plus efficace de leur époque. Elles visent un idéal : rassembler le plus grand nombre possible de savoirs, les mettre en relation, les transmettre, les partager, les soumettre à la discussion. Voilà pourquoi leurs créateurs ont « bien mérité du genre humain » (Diderot).
> 
> <cite>Melançon, Benoît. Les vertus utopiques de l’encylopédisme. 2018. Sens public. http://sens-public.org/articles/1323/</cite>

Chargé de valorisation de l’ENCCRE, notamment auprès des publics enseignants et scolaires, j’avais particulièrement apprécié ce scénario pédagogique de Nicolas Bannier, professeur de lettres dans l’académie de Strasbourg, qui a engagé des élèves d’une classe de seconde dans une activité qui jette des ponts entre les deux encyclopédies : <https://www.ac-strasbourg.fr/pedagogie/lettres/enseigner-avec-le-numerique/ressources-numeriques/de-lencyclopedie-a-wikipedia/>. L’auteur s’est entretenu à ce sujet avec mon estimé collègue Jean-Michel Le Baut dans le Café pédagogique, en novembre 2018, dans cet article intitulé [« Vers des lycéens encyclopédistes ? »](http://www.cafepedagogique.net/lexpresso/Pages/2018/11/12112018Article636776045275945091.aspx).

Plus généralement, Wikipedia a su reconquérir une place [qui ne lui était pas acquise au départ à l’Éducation nationale](https://www.franceculture.fr/amp/numerique/wikipedia-vingt-ans-apres-sa-creation-oscille-toujours-entre-defiance-et-acceptation-aupres-des). Après avoir longtemps eu mauvaise presse auprès des enseignant⋅es, l’encyclopédie en ligne est aujourd’hui le support de plusieurs concours ou activités. Le CLEMI et Wikimedia France organisent chaque année depuis 8 ans maintenant le [Wiki\_concours lycéen](https://www.clemi.fr/fr/evenements/concours/wiki-concours-lyceen.html), qui propose à des élèves de classes de lycées de créer ou d’enrichir des contenus Wikipedia (voir également [la page du projet](https://fr.wikipedia.org/wiki/Projet:Wikiconcours_lyc%C3%A9en) sur WP). Le lycée Kerichen-La Pérouse à Brest en a été [lauréat en 2018](https://fr.wikipedia.org/wiki/Projet:Wikiconcours_lyc%C3%A9en_2017-2018/Acad%C3%A9mie_de_Rennes/Kerichen). Plusieurs ressources viennent à l’appui des professeur⋅es pour organiser des activités autour de Wikipedia : mentionnons ce Wikilivre, « [Wikipedia en éducation](https://fr.wikibooks.org/wiki/Wikip%C3%A9dia_en_%C3%A9ducation)« , qui donne des pistes d’exploitation du potentiel de l’encyclopédie en classe, et le portail <https://education.wikimedia.fr/>.

Quant à ma propre expérience de contributeur, elle est très limitée, et mes interventions sur l’encyclopédie en ligne sont hélas trop rares et irrégulières aujourd’hui. Elle a débuté en 2017 avec le [WikiMOOC](https://fr.wikipedia.org/wiki/Projet:WikiMOOC), formation en ligne qui m’a permis de comprendre les enjeux du projet Wikipedia et d’acquérir les bases de l’édition d’un article. À cette occasion j’ai créé la page d’[Elisabeth Ferrand](https://fr.wikipedia.org/wiki/%C3%89lisabeth_Ferrand), salonnière française de la première moitié du XVIIIe siècle (au passage, prenez le temps d’aller voir et de soutenir l’initiative [Les sans pagEs](https://sanspages.org/) qui vise à « réduire le fossé des genres sur Wikipedia par la production d’articles sur l’encyclopédie, et l’organisation d’évènements et de formations pour promouvoir la participation et la visibilité des femmes aux projets Wikimedia »). J’ai aussi initié deux autres pages sur des sujets sur lesquels je me sentais très légitime et confortable, puisqu’il s’agit de mon sujet de thèse de doctorat, sur lequel j’ai travaillé six ans : la page sur [Gabriel Cramer](https://fr.wikipedia.org/wiki/Gabriel_Cramer) et celle concernant son [ouvrage sur les courbes algébriques](https://fr.wikipedia.org/wiki/Introduction_%C3%A0_l%27analyse_des_lignes_courbes_alg%C3%A9briques). Ces deux articles, assez longs, ont été rédigés par itérations successives, et ont été enrichis et améliorés par les remarques et corrections de fond et de forme apportées par d’autres contributeurs qui m’ont beaucoup appris sur le fonctionnement de l’encyclopédie en ligne au fil de nos discussions ou de leurs conseils.

Il me reste maintenant à contribuer sur d’autres sujets, relire des articles, apporter mes premières corrections, participer aux discussions, bref être un contributeur plus régulier sur le projet lui-même, et pas seulement sur les sujets dont je maîtrise le fond. Peut-être en intégrant une communauté de wikipédien⋅nes locale, lorsque la crise sanitaire sera derrière nous, et que je trouverai un peu de temps pour m’y consacrer ?

Joyeux anniversaire Wikipedia !

{{< youtube id="PzGAGfSObOw" title="Wikipedia’s 20th birthday party !" >}}


**Pour aller plus loin**

- chez Olivier Ertzscheid : [https://www.affordance.info/mon\_weblog/2021/01/tous-les-jours-20-ans-wikipedia.html](https://www.affordance.info/mon_weblog/2021/01/tous-les-jours-20-ans-wikipedia.html)
- sur lemonde.fr / Pixels (Nicolas Six) : [https://www.lemonde.fr/pixels/article/2021/01/14/wikipedia-fete-les-20-ans-d-une-belle-et-exigeante-utopie\_6066271\_4408996.html](https://www.lemonde.fr/pixels/article/2021/01/14/wikipedia-fete-les-20-ans-d-une-belle-et-exigeante-utopie_6066271_4408996.html)
