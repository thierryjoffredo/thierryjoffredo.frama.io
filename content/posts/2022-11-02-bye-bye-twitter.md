---
title: 'Bye Bye Twitter ; so long, and thanks for all the fish !'
date: '2022-11-02T09:15:18+01:00'
author: thierry
layout: post
permalink: /2022/11/02/bye-bye-twitter/
categories:
    - 'Comme ça vient'
---

Le rachat de Twitter, finalisé par Elon Musk à la fin du mois d’octobre, a scellé ma décision de quitter ce réseau sur lequel j’étais actif depuis plus de 10 ans. Sa fréquentation était déjà de plus en plus difficile depuis deux-trois ans, il était devenu très compliqué de ne plus s’exposer aux discours haineux, complotistes, ou tout simplement stupides, même en paramétrant finement son compte, en masquant certaines personnes ou contenus, et en choisissant ses abonnements… Une toxicité à petite dose, mais qui commençait à produire des effets franchement indésirables chez moi. Les choses ne vont maintenant qu’empirer, et l’idée a fait son chemin : il est donc temps de plier bagage et de regarder ailleurs. Par ailleurs, un peu de cohérence ne nuit pas, et critiquer les plateformes de l’intérieur a ses limites : dans le cas de Twitter, cela revient même à lui donner du carburant. J’ai donc demandé une archive de mes données, et supprimerai mon compte au plus tard mi-novembre[^1].

{{< figure src="/wp-content/uploads/2022/11/Screenshot-2022-11-02-at-08-38-20-Elon-Musk-sur-Twitter.png" caption="And demons are unleashed…" >}}

Je vais toutefois regretter de ne plus entendre des voix qui m’accompagnaient, pour certaines, depuis 10 ans. Des personnes sensibles, engagées, cultivées, drôles, en un mot précieuses, que j’avais pris l’habitude de lire régulièrement, sans nécessairement interagir. Je sais qu’il existe des moyens de jeter un œil aux timelines Twitter publiques (via Nitter par exemple), mais pour les comptes privés, cela ne sera hélas plus possible. En espérant pouvoir les lire ou les écouter ailleurs (blogs, autres réseaux…).

Parmi ceux qui m’ont tracé la voie et permis de prendre cette décision, il y a eu Louis Derrac (à lire ici : <https://louisderrac.com/2022/10/31/au-revoir-twitter/> – vous trouverez d’ailleurs des liens vers d’autres témoignages en fin de billet) mais surtout Ploum, qui a su parfaitement toucher au cœur avec ce texte : <https://ploum.net/la-legerete-dun-monde-sans-twitter/> :

> Il est simple de se retirer des réseaux que l’on n’aime pas ou qu’on utilise peu. Le réel changement vient d’accepter de se retirer d’un réseau dont on connait la nocivité, pour soi et pour le monde, sans pourtant pouvoir s’en passer. Parce qu’on est persuadé d’en tirer plus de bien que de mal.
> 
> <cite>https://ploum.net/la-legerete-dun-monde-sans-twitter/</cite>

Va donc pour un peu plus de légèreté ! Nous verrons si l’aventure Mastodon / [Fédivers](https://www.miscellanea-numerica.fr/2022/04/24/petite-introduction-au-fedivers/), que j’ai commencée au printemps 2017, jusqu’à aujourd’hui très tranquille et agréable, me permettra aussi d’entendre d’autres voix – ou de retrouver celles que je quitte, ce qui n’est pas impossible au vu de l’exode actuel des utilisateurs de Twitter vers Mastodon. Je dois dire que je vois arriver avec plaisir des profils différents de ceux qui y étaient actifs jusque là, et que j’apprécie énormément, mais qui étaient très homogènes (développeurs, activistes). Aujourd’hui arrivent par dizaines des astronomes, des historien⋅nes, des artistes, des enseignant⋅es, des linguistes, des musicien⋅nes, … Pour éviter des déceptions ou des déconvenues à ces nouveaux et nouvelles venu⋅es : ne pensez pas que Mastodon / le fédivers soit *par nature* vertueux, accueillant, sécurisant. En revanche, on y trouve moins de fonctionnalités favorisant les comportements toxiques (recherche limitée, nombre d’interactions moins visibles, pas de citation de post…), pas de publicité/profilage, pas d’algorithme décidant à votre place des contenus pertinents… et l’incroyable potentiel de devenir ce qu’on voudra bien, collectivement, en faire, instance par instance, en reprenant la main.

Retrouvez-moi donc sur <https://mamot.fr/@ThierryJoffredo>, si vous le souhaitez, et prenez le temps de faire un détour par ce pad pour vous guider dans vos premiers pas sur le fédivers : <https://mypads2.framapad.org/p/twitter-mastodon-9c2lz19ed> (merci à Louis de l’avoir ouvert et de le maintenir).

[^1]: **Mise à jour 4/11** : voilà, compte désactivé ! J’ai sauté le pas plus tôt que prévu.
