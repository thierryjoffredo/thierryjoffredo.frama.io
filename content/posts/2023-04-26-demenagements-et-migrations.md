---
title: 'Déménagements et migrations'
date: '2023-04-26'
categories:
    - 'Comme ça vient'
---

Cela faisait près de six ans que j'avais élu domicile (numérique) chez Gandi, avec un hébergement Simple hosting de 20 Go qui m'a permis de tester plein de choses : des noms de domaine à gérer, un Nextcloud partagé pour la famille (documents, photos, agendas...), une instance de TinyTinyRSS pour ma veille, un espace Cozy Cloud hébergé (mais fermé à l'automne), et deux ou trois sites motorisés par Wordpress. C'est intéressant, et même nécessaire, de mettre les mains dans le cambouis pour apprendre comment tout ça fonctionne, mais ça prend du temps et ce n'est pas toujours simple : mettre à jour un Nextcloud, par exemple, n'est pas sans risque, comme j'ai pu l'expérimenter. J'ai donc décidé récemment de réduire un peu la voilure, et de plutôt utiliser des services déjà existants, proposés et opérés par des personnes qui savent ce qu'elles font, à qui je fais confiance, pour abriter mon chez-moi numérique : [Cozy](https://cozy.io) pour mon cloud familial. [Flus](https://app.flus.fr) pour ma [veille](/posts/2022-05-07-repenser-sa-veille-flus), et Framasoft pour mes agendas et mon espace de publication web. C'est de ce dernier point dont je voulais garder trace ici

## Une alternative à Wordpress : Hugo
Je souhaitais, par simple curiosité, explorer des alternatives (toujours libres et open source) à Wordpress pour ma page web. J'avais déjà un peu exploté [Plume](https://joinplu.me/fr-fr/) il y a deux ou trois ans, qui avait le mérite certain d'être intégré au fédiverse et donc, par le biais du protocole ActivityPub, de s'articuler avec d'autres services que j'utilise, comme Mastodon. Néanmoins, le service n'était pas complètement mûr et surtout, vous le constaterez en visitant la page [https://joinplu.me/fr-fr/](https://joinplu.me/fr-fr/), au moment où j'écris ce billet, il n'est plus activement maintenu. Cette expérience a eu le mérite de commencer à me faire rédiger mes billets en [Markdown](https://www.markdownguide.org/getting-started/), ce que j'ai trouvé très satisfaisant : la légèreté du format texte, un langage simple à appréhender, des possibilités de conversion et de rendu très diverses (pdf, html)… Mon attention s'est alors récemment portée sur [Hugo](https://gohugo.io/), qui permet de créer assez facilement des sites statiques assez légers (et, donc, sans base de données) à partir de contenus rédigés en markdown, avec une [communauté](https://discourse.gohugo.io/) plutôt vivante et active, et des [thèmes](https://themes.gohugo.io/) plutôt attractifs. 

Après une première prise en main, qui m'a permis d'héberger une simple page personnelle de présentation sur mon serveur Gandi, j'ai décidé d'aller un peu plus loin et de créer le site sur lequel vous êtes en train de lire ce billet, et d'y migrer tous mes billets wordpress. Je ne souhaitais pas forcément perdre mes articles, ni les réécrire complètement, aussi j'ai installé l'[extension Jekyll Exporter](https://wordpress.com/plugins/jekyll-exporter) pour convertir mes articles wordpress en fichiers markdown, que j'ai ensuite réintégrés sur mon nouveau site Hugo (avec un peu de reprise, tout de même).

## Un articulation bienvenue avec Gitlab Pages
Depuis plusieurs mois maintenant, je gardais un œil sur les forges numériques, que je pensais un peu réservées aux usages de développeurs - même si j'avais fait une incursion en 2016 pour y héberger et versionner les fichiers LaTeX de ma thèse de doctorat en cours d'écriture. J'avais alors ouvert un projet [these sur Framagit](https://framagit.org/thierryjoffredo/these), mais n'y étais plus revenu après ma soutenance, fin 2017. Voyant ce que d'autres arrivaient à en faire, plus particulièrement Cédric Eyssette, professeur de philosophie dans l'académie de Lyon (voir [ici](https://github.com/eyssette) et [là](https://eyssette.github.io/), par exemple), et plus particulièrement les possibilités de publication web avec les [Gitlab Pages](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html), je me suis décidé à y regarder de plus près. Et voyant que [Gitlab offre un support natif pour Hugo](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/), bien documenté, j'ai vu là l'opportunité de venir héberger mon site sur Framagit, qui a vu le jour début avril 2023. 

## Comment je m'y suis pris
Attention : un prérequis est de savoir saisir et manipuler des lignes de commande dans un terminal, et d'avoir quelques notions sur [git](https://docs.framasoft.org/fr/gitlab/1-introduction.html). Le principe général est d'avoir, localement sur son poste, un répertoire pour accueillir son site Hugo, pouvoir y gérer les fichiers de configuration et rédiger les fichiers Markdown supports des contenus (pages, billets...), de visualiser localement son site au fur et à mesure des modifications et ajouts avant de le publier sur son hébergement distant. Les étapes que j'ai suivies ont été les suivantes.

### Création du site en local (bac à sable)
Il s'agit ici de vous guider dans la création d'un site Hugo "Bac à sable" en local sur votre poste. La mise en ligne sera abordée dans la partie suivante.

1. Installez git et hugo sur votre machine (Linux, en ce qui me concerne) : voir [https://gohugo.io/installation/](https://gohugo.io/installation/) ;
2. Créez un répertoire MonSite sur votre poste, et initialisez le site ; pour cela, ouvrez un terminal, et saisissez les commandes :

        cd /MonSite
        hugo new site ExempleSiteHugo

ce à quoi il devrait répondre quelque chose du style : 

        Congratulations! Your new Hugo site is created in /home/<user>/MonSite/ExempleSiteHugo.
        Just a few more steps and you're ready to go:
        1. Download a theme into the same-named folder.
           Choose a theme from https://themes.gohugo.io/ or
           create your own with the "hugo new theme <THEMENAME>" command.
        2. Perhaps you want to add some content. You can add single files
           with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
        3. Start the built-in live server via "hugo server".
        Visit https://gohugo.io/ for quickstart guide and full documentation.
        
3. Choisir un [thème](https://themes.gohugo.io/) et l'installer ; j'ai choisi [PaperMod](https://themes.gohugo.io/themes/hugo-papermod/) dont les instructions d'installation sont [ici](https://github.com/adityatelange/hugo-PaperMod/wiki/Installation). Je n'y suis pas arrivé avec les deux premières méthodes, j'ai donc utilisé la troisième : téléchargement du thème et extraction dans le dossier `/ExempleSiteHugo/themes` Vous devez maintenant avoir sur votre poste une arborescence de ce type : 

![](/mediatheque/arborescence.png#center)
4. À la racine, le fichier `config.toml` vous permettra de paramétrer votre site. Pour l'instant, il doit se présenter de cette manière : 

        baseURL = 'http://example.org/'
        languageCode = 'en-us'
        title = 'My New Hugo Site'
        
que vous pouvez déjà modifier en :

        baseURL = 'http://example.org/'
        languageCode = 'fr-fr'
        title = 'Mon nouveau site Hugo'
        theme = 'hugo-PaperMod-master'
        
le nom du thème est celui du dossier présent dans `/themes`     
5. Jetez un œil à votre nouveau site, en local, en saisissant, dans le terminal : 
        
        hugo server
        
et en ouvrant un onglet de votre navigateur sur l'url indiquée dans le message reçu en réponse (chez moi, http://localhost:1313/).

6. Configurez votre site ; le concepteur du thème PaperMod préconise d'utiliser un fichier `config.yml` plutôt que le fichier `config.toml` fourni par défaut. Pas de problème, supprimez ce fichier toml et créez un nouveau fichier `config.yml` puis complétez-le avec les lignes suivantes, comme préconisé [ici](https://github.com/adityatelange/hugo-PaperMod/wiki/Installation) (avec quelques ajustements de ma part ; remarquez que la syntaxe change un peu par rapport au format toml):

        baseURL: "https://examplesite.com/"
        title: Mon nouveau site Hugo
        paginate: 5
        theme: hugo-PaperMod-master

        enableRobotsTXT: true
        buildDrafts: false
        buildFuture: false
        buildExpired: false

        #googleAnalytics: UA-123-45

        minify:
          disableXML: true
          minifyOutput: true

        params:
          env: production # to enable google analytics, opengraph, twitter-cards and schema.
          title: Mon nouveau site Hugo
          description: "Description"
          keywords: [Blog, Portfolio, PaperMod]
          author: Mon nom
          # author: ["Me", "You"] # multiple authors
          images: ["<link or path of image for opengraph, twitter-cards>"]
          DateFormat: "2 January 2006"
          defaultTheme: auto # dark, light
          disableThemeToggle: false

          ShowReadingTime: true
          ShowShareButtons: true
          ShowPostNavLinks: true
          ShowBreadCrumbs: true
          ShowCodeCopyButtons: false
          ShowWordCount: true
          ShowRssButtonInSectionTermList: true
          UseHugoToc: true
          disableSpecial1stPost: false
          disableScrollToTop: false
          comments: false
          hidemeta: false
          hideSummary: false
          showtoc: false
          tocopen: false

          assets:
            # disableHLJS: true # to disable highlight.js
            # disableFingerprinting: true
            favicon: "<link / abs url>"
            favicon16x16: "<link / abs url>"
            favicon32x32: "<link / abs url>"
            apple_touch_icon: "<link / abs url>"
            safari_pinned_tab: "<link / abs url>"

          label:
            text: "Home"
            icon: /apple-touch-icon.png
            iconHeight: 35

          # profile-mode
          profileMode:
            enabled: true # needs to be explicitly set
            title: Mon nouveau site Hugo
            subtitle: "un sous-titre"
            imageUrl: "<img location>"
            imageWidth: 120
            imageHeight: 120
            imageTitle: Mon image
            buttons:
              - name: Posts
                url: posts
              - name: Tags
                url: tags

          # home-info mode
          homeInfoParams:
            Title: "Hi there \U0001F44B"
            Content: Welcome to my blog

          socialIcons:
            - name: twitter
              url: "https://twitter.com/"
            - name: stackoverflow
              url: "https://stackoverflow.com"
            - name: github
              url: "https://github.com/"

          #analytics:
            #google:
              #SiteVerificationTag: "XYZabc"
            #bing:
              #SiteVerificationTag: "XYZabc"
            #yandex:
              #SiteVerificationTag: "XYZabc"

          cover:
            hidden: true # hide everywhere but not in structured data
            hiddenInList: true # hide on list pages and home
            hiddenInSingle: true # hide on single page

          #editPost:
            #URL: "https://github.com/<path_to_repo>/content"
            #Text: "Suggest Changes" # edit text
            #appendFilePath: true # to append file path to Edit link

          # for search
          # https://fusejs.io/api/options.html
          fuseOpts:
            isCaseSensitive: false
            shouldSort: true
            location: 0
            distance: 1000
            threshold: 0
            minMatchCharLength: 3
            keys: ["title", "permalink", "summary", "content"]
        menu:
          main:
            - identifier: blog
              name: Blog
              url: /posts/
              weight: 10
            - identifier: page
              name: Page
              url: /page/
              weight: 20
            - identifier: lien
              name: example.org
              url: https://example.org
              weight: 30
        # Read: https://github.com/adityatelange/hugo-PaperMod/wiki/FAQs#using-hugos-syntax-highlighter-chroma
        pygmentsUseClasses: true
        markup:
          highlight:
            noClasses: false
            # anchorLineNos: true
            # codeFences: true
            # guessSyntax: true
            # lineNos: true
            # style: monokai
            

7. Jetez un œil à votre terminal, dans lequel le process `hugo server` doit toujours tourner ; vous devriez voir un message du type : 

        Change of config file detected, rebuilding site.
        2023-04-26 10:54:34.925 +0200
        Rebuilt in 73 ms

et revenez sur votre onglet de navigateur à l'url http://localhost:1313/ : si tout s'est bien passé, vous devriez avoir cet aperçu de votre site en local : 

![](/mediatheque/Mon_nouveau_site_Hugo.png#center)

8. Créez une page puis un billet, d'abord en ajoutant un fichier nommé `page1.md` à la racine du dossier `content`, contenant : 

        # Page 1
        
        Contenu en markdown

puis créez un dossier `posts` dans le dossier `content`et placez-y un fichier nommé `billet1.md` (ou tout autre nom que vous souhaitez) contenant : 

        ---
        title: 'Billet n°1'
        date: '2023-04-26'
        categories:
            - 'Catégorie 1'
        ---
        # Mon premier billet
        Contenu en markdown

N'importe quel éditeur de texte convient ; pour ma part, j'utilise régulièrement [Zettlr](https://www.zettlr.com/) pour rédiger des contenus en markdown. 

En revenant sur l'aperçu de votre site dans votre onglet de navigateur, vous devriez maintenant pouvoir cliquer sur les liens "Page" et "Blog" en haut à droite pour voir s'afficher les contenus que vous venez de créer ! 

9. Maintenant que le site est correctement paramétré, vous devez créer les pages statiques qui seront présentées à vos visiteurs et visiteuses. Revenez sur le terminal, tapez `CTRL+C` pour arrêter le process en cours, puis tapez simplement la commande : 

        hugo

Si tout se passe bien, il doit vous répondre quelque chose comme : 

        Start building sites … 
        hugo v0.92.2+extended linux/amd64 BuildDate=2023-01-31T11:11:57Z VendorInfo=ubuntu:0.92.2-1ubuntu0.1

                           | EN  
        -------------------+-----
          Pages            | 14  
          Paginator pages  |  0  
          Non-page files   |  0  
          Static files     |  0  
          Processed images |  0  
          Aliases          |  2  
          Sitemaps         |  1  
          Cleaned          |  0  

        Total in 70 ms

Vous devez maintenant retrouver un nouveau dossier `public` dans votre arborescence locale, dans lequel ont été générés tous les fichiers html permettant à vos visiteurs de consulter votre site. Reste à mettre en ligne ce site, maintenant. Soit vous disposez d'un hébergement, et vous pouvez simplement y copier le contenu du dossier `public` par ftp, par exemple. Soit vous utilisez l'intégration dans Gitlab Pages sur une forge comme Framagit, ce que je vais détailler dans la partie suivante.

### Mise en ligne sur Framagit

**Prérequis** : disposer d'un compte Framagit[^1] ([Procédure](https://docs.framasoft.org/fr/gitlab/2-creation-configuration-compte.html)), avoir créé une clé SSH et initalisé le Git local ([Procédure](https://docs.framasoft.org/fr/gitlab/3-installation-poste.html)). 

 1. Ouvrez un nouveau projet sur Framagit. Choisissez "Create from template" et cherchez la ligne "Pages/Hugo". Cliquez sur le bouton "Utilisez ce modèle" en bout de ligne. Dans le champ "Nom du projet", mettez "ma-page", par exemple (ou votreidentifiant.frama.io si vous souhaitez que votre site soit directement accessible par cette url à l'avenir).
 2. Attendez que le modèle se charge, vous devriez voir ceci après quelques secondes : 

![](/mediatheque/framagit_creationProjetHugo.png#center)
3. Cliquez sur le bouton bleu "Cloner" et choisissez "Clonez avec SSH" (copiez l'url)
4. Rouvrez un terminal, et rendez-vous dans votre dossier `MonSite`

        cd /MonSite

5. Clonez le projet que vous venez d'ouvrir sur Framagit en saisissant `git clone` plus l'url contenue dans votre presse-papier (à l'étape 3) : 

        git clone git@framagit.org:votreidentifiant/ma-page.git

commande qui doit répondre quelque chose comme : 

        Clonage dans 'ma-page'...
        remote: Enumerating objects: 30, done.
        remote: Total 30 (delta 0), reused 0 (delta 0), pack-reused 30
        Réception d'objets: 100% (30/30), 23.62 Kio | 2.15 Mio/s, fait.
        Résolution des deltas: 100% (1/1), fait.

un nouveau dossier `ma-page` a été créé dans le dossier `MonSite` de votre poste !

![](/mediatheque/arborescence2.png#center)


6. Sur votre poste, supprimez le fichier `config.toml` et remplacez-le par le fichier `config.yml` que vous avez édité dans votre site bac à sable (`/MonSite/ExempleSiteHugo/config.yml)`. Modifiez la première ligne pour adapter le paramètre baseURL : `https://votreidentifiant.frama.io/ma-page` ou tout simplement `https://votreidentifiant.frama.io` si vous avez choisi de nommer votre projet `votreidentifiant.frama.io`. Puis copiez le contenu des dossiers `/MonSite/ExempleSiteHugo/content` et `/MonSite/ExempleSiteHugo/themes` dans les dossiers `/MonSite/ma-page/content` et `/MonSite/ma-page/themes` respectivement. Ce faisant, les contenus de notre site bac à sable local (que vous pouvez conserver pour explorer Hugo et faire des essais) sont utilisés pour initier notre "vrai" site qui sera très bientôt en ligne.  
7. Revenez à votre terminal, et placez-vous dans le dossier `/ma-page` 

        cd /ma-page

puis lancez la commande 

        hugo server
        
et rendez-vous sur votre navigateur à l'url indiquée (http://localhost:1313/) pour vérifier que vous retrouvez bien l'affichage de votre site bac à sable, comme à l'étape 7 de la première partie. Si tout est bon, stoppez le process en tapant `CTRL+C` et générez les pages publiques du site : 

        hugo

Sur votre poste, le dossier  `/MonSite/ma-page/public`  a dû êtré créé.

8. Reste à envoyer tout ça en ligne sur le projet Framagit ; dans le terminal saisissez successivement les commandes : 

        git add --all
        git commit -m "Mise en ligne de ma page Hugo"
        git push

Si tout se passe bien, votre projet Framagit "ma-page" doit maintenant ressembler à ça : 

![](/mediatheque/framagit_updateProjetHugo.png)
9. Ouvrez un nouvel onglet de navigateur et saisissez l'url de votre page : https://votreidentifiant.frama.io/ma-page (ou simplement https://votreidentifant.frama.io si vous avez choisi d'appeler votre projet votreidentifiant.frama.io, comme proposé à l'étape 1). Et voilà ! Pour la suite, il ne vous reste plus qu'à gérer vos contenus (accueil, pages, billets, liens...) sur votre poste, localement, avant de réitérer les étapes 6 à 8 pour mettre à jour votre site. Pour cela, référez-vous aux documentations [sur le site de Hugo](https://gohugo.io/documentation/) ou sur [celui du thème PaperMod](https://themes.gohugo.io/themes/hugo-papermod/).

Voici ce que ça donne pour moi : https://thierryjoffredo.frama.io/ma-page/. Vous pouvez retrouver le projet ma-page tel qu'il a été réalisé en rédigeant ce billet, sur mon compte Framagit, à cette adresse : https://framagit.org/thierryjoffredo/ma-page. Vous pouvez également aller voir le détail du projet de mon "vrai" site ici : https://framagit.org/thierryjoffredo/thierryjoffredo.frama.io. Si vous vous lancez de votre côté en suivant les instructions de ce billet, et que vous souhaitez échanger ou poser des questions, contactez-moi par mail ou sur Mastodon.

[^1]: Et puisqu'on utilise un service Framasoft, on n'oublie pas d'aller soutenir l'association par un don : [https://soutenir.framasoft.org/fr/](https://soutenir.framasoft.org/fr/)