---
id: 1281
title: "«\_Cultiver son potager de bits\_»"
date: '2021-04-20T09:43:51+02:00'
author: thierry
layout: post
guid: 'https://www.miscellanea-numerica.fr/?p=1281'
permalink: /2021/04/20/cultiver-son-potager-de-bits/
categories:
    - 'Comme ça vient'
---

Je vous recommande le courte mais revigorante lecture de ce texte de Stéphane Crozat, professeur à l’UTC et membre de Framasoft, paru cette semaine dans les pages de NextInpact, intitulé [Connaître les machines, une question d’autonomie pour les humains](https://www.nextinpact.com/article/46800/connaitre-machines-question-dautonomie-pour-humains).

Rappelant que, pour le philosophe Gilbert Simondon (que je n’ai pas lu), « la méconnaissance de la machine est la cause majeure de l’aliénation du monde contemporain » et que la solution à ce problème passe par l’inclusion de la technique dans la culture, il fait le constat suivant, que je partage pleinement :

> Et on réalise en 2021, soixante ans après Simondon, trente ans après Berners-Lee, qu’on a oublié un truc en chemin. Notre autonomie. La connaissance des machines est affaire de culture et non d’ingénierie, les machines construisent les hommes autant que l’inverse. Quand l’humain tient le marteau et le burin, ce ne sont pas de simples outils qui obéissent à sa volonté, ils façonnent celui qui les tient.
> 
> <cite>Stéphane Crozat, NextInpact, 16 avril 2021, <https://www.nextinpact.com/article/46800/connaitre-machines-question-dautonomie-pour-humains></cite>

L’auteur en appelle donc à l’éducation populaire pour s’armer et se défendre contre l’aliénation, l’asservissement, l’addiction, la prolétarisation, la décapacitation d’utilisateurs en phase « terminal » et se réapproprier nos outils numériques pour les adapter à nos besoins et à nos désirs, à notre propre bénéfice – et non celui de vendeurs de matériels, d’attention ou de publicité. Nul doute que l’Éducation nationale a également son rôle à jouer pour former les plus jeunes, les futur⋅es citoyen⋅nes de ce pays, à cette culture numérique et les aider à être plus exigeants et vigilants que notre génération l’a été, et à faire infuser cette nécessaire capacité d’auto-défense numérique dans leurs familles et milieux amicaux.

Cela passe par des pratiques éclairées, et pas seulement des procédures et des tutoriels pour parvenir à des fins sans en comprendre les moyens. Il s’agit donc, collectivement, solidairement, de reprendre le pouvoir, par la connaissance et la pratique, pour s’informer, construire, agir :

> Que chacun devienne un peu plus artisan de ses propres usages. C’est l’idée du jardinage appliqué au numérique, avoir un bout de jardin pour savoir comment ça pousse, choisir de ne pas manger certains produits de l’industrie agro-alimentaire, faire des échanges avec ses voisins. **Cultiver un potager de bits**.
> 
> <cite><https://www.nextinpact.com/article/46800/connaitre-machines-question-dautonomie-pour-humains></cite>

D’ailleurs l’auteur nous engage dans cette démarche grâce à son cours intitulé « Écrire, communiquer et collaborer sur le web » qu’il met à disposition du grand public sur la plateforme Librecours : <https://librecours.net/parcours/we01-001/index.html>

L’objectif de ce cours est de « participe\[r\] de la **littératie numérique** des citoyens afin de leur permettre d’acquérir une **autonomie** dans la compréhension des concepts et la manipulation des outils afin d’exercer pleinement leur rôle dans leurs projets professionnels et personnels et de diminuer leur dépendance à l’expertise technique exogène. L’enjeu est de renforcer l’articulation entre les informaticiens qui décident de fait ce qui est possible et souhaitable, parce ce qu’ils ont la capacité technique de faire, et le concepteur ou l’utilisateur qui essayent de vivre le monde numérique indépendamment de son identité technique. »

Je parcours en ce moment même les pages de ce librecours, qui « aborde les technologies du web sous les angles théoriques (théorie du support et du document), pratiques (machines et langages), méthodologiques (outils pour la collaboration en ligne et agilité) et culturels (usages et économie) ». Je vous recommande notamment d’écouter les cours-émissions, diffusés sur la radio Graf’hit, et disponibles sur cette page : <https://librecours.net/parcours/we01-001/peertube.html>.

Tous au jardin !

Site internet de Stéphane Crozat : <https://aswemay.fr/co/news.html>  
Le suivre sur Mastodon : <https://framapiaf.org/@stph> ou Twitter : <https://twitter.com/StphCrozat>
