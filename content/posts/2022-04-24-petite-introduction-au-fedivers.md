---
title: 'Petite introduction au fédivers'
date: '2022-04-24T17:17:03+02:00'
author: thierry
layout: post
permalink: /2022/04/24/petite-introduction-au-fedivers/
categories:
    - 'Comme ça vient'
---

*Ce billet reprend pour l’essentiel le contenu d’une présentation du fédivers que je ferai le mercredi 27 avril après-midi dans un cadre professionnel, à la demande du CLEMI de l’académie de Rennes.* *N’hésitez pas à me signaler d’éventuelles erreurs ou des imprécisions à corriger dans les commentaires (ou sur [Mastodon](https://mamot.fr/@ThierryJoffredo) :-))*.

## Des réseaux sociaux centralisés qui posent des questions {#reseaux}

Les réseaux sociaux comme Twitter, Facebook, Instagram (Meta) ou Youtube (Google) – pour ne parler que des services proposés par de grandes multinationales américaines, et couramment utilisés dans le monde occidental – regroupent aujourd’hui des centaines de millions, voire des milliards d’utilisateurs. Les effets délétères de ces grands réseaux sociaux sont aujourd’hui connus et documentés :

- Utilisation systématique d’algorithmes (fermés) de suggestion, amplification de la diffusion de théories complotistes / fausses informations et possibles manipulations de l’opinion, valorisation de l’outrance et polarisation des débats ;
- Modèle basé sur l’économie de l’attention, l’exploitation des données personnelles[^1] et l’exposition à de la publicité, captivité par effet de réseau ;
- Difficultés liées à la modération (largement automatisée) des contenus, décisions unilatérales sur l’exercice de la liberté d’expression…

Très récemment, des inquiétudes se sont manifestées autour, par exemple, de la volonté de prise de contrôle de Twitter par Elon Musk[^2]. La centralisation de ces réseaux aujourd’hui *de facto* incontournables, aux mains de quelques dirigeants qui prennent des décisions selon leurs propres intérêts, ceux de leurs actionnaires ou des états qui entendent les réguler, pose des questions importantes pour notre vie démocratique. En voici quelques exemples issus de l’actualité récente :

{{< figure src="/wp-content/uploads/2022/04/Capture-decran-du-2022-04-24-15-35-56-1024x554.png" >}}

Pourtant, on sait aussi, par expérience ou par des exemples historiques, que les réseaux sociaux peuvent être sources d’expériences positives et de bienfaits :

- partage d’expériences et de savoirs, diffusion des idées, échanges et confrontation / construction d’opinions ;
- liberté d’expression et de circulation de l’information ;
- organisation des solidarités et des résistances[^3] ;
- etc.

**Alors, comment peut-on reprendre le contrôle ?**

Même si elles ne résoudront aucun de nos « problèmes démocratiques 2.0 » par elles-mêmes, il est intéressant de savoir que des alternatives décentralisées à ces différents réseaux sociaux existent, et peuvent offrir des perspectives plus optimistes : taille raisonnable et maîtrisable (pour des questions de modération par exemple), absence de publicité ou d’algorithme de mise en valeur de contenus non choisis, respect de la vie privée et des données personnelles de leurs utilisateurs, etc. **Bienvenue, donc, dans le fédivers** !

{{< figure src="/wp-content/uploads/2022/04/Capture-decran-du-2022-04-26-07-35-02-1024x409.png" caption="Représentation schématique (et personnelle) du Fedivers et de quelques-un de ses principaux services." >}}

## Qu’est ce que le fédivers ? {#fedivers}

Voici la définition qui en est donnée sur le Wikipedia francophone :

> **Fediverse** (ou Fédiverse, parfois orthographié Fedivers ou Fédivers) est un [mot-valise](https://fr.wikipedia.org/wiki/Mot-valise) de l’anglais pour « fédération » et « univers ». C’est un nom informel pour désigner une assez large [fédération](https://fr.wikipedia.org/wiki/F%C3%A9d%C3%A9ration_(informatique)) de serveurs formant un réseau social. Il est construit autour de [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre), permettant donc un [auto-hébergement](https://fr.wikipedia.org/wiki/Auto-h%C3%A9bergement_(Internet)), ou bien l’utilisation d’un service prêt à l’emploi chez un tiers. Les différents services disponibles pour les nœuds de ces instances sont hétérogènes, microblog, blog, vidéo, image, articles de recherche, code logiciel, mais utilisent des protocoles d’échanges communs pour communiquer entre eux, « se fédérer », ou des ponts entre différents protocoles de façon transparente pour l’utilisateur, la volonté étant de fournir une alternative ouverte et résiliente aux réseaux sociaux captifs, propriétés d’une unique entité.
> 
> <cite><https://fr.wikipedia.org/wiki/Fediverse></cite>

Ce fédivers est donc structuré par plusieurs services qui permettent à ses utilisateurs – les fédinautes – de partager des contenus (microblogging, partage d’images, de vidéos, blog…) et qui communiquent entre eux grâce à un protocole commun (appelé [ActivityPub](https://fr.wikipedia.org/wiki/ActivityPub), pour l’essentiel). Ces services libres et open-source, dont les plus connus (de moi, en tous cas !) sont Mastodon (proche de Twitter), <s>Diaspora (proche de Facebook)</s>[^4], PixelFed (partage d’images, proche d’Instagram), Plume (blog), Mobilizon (organisation d’événements) ou Peertube (partage de vidéos, proche de Youtube), sont hébergés par des particuliers, des associations, des collectifs voire des administrations sur leurs propres serveurs (on parle ici d’*instances*) et s’adressent à des communautés d’utilisateurs qui peuvent être très diverses, avec des conditions générales d’utilisation et des politiques de confidentialité qui leur sont propres (à lire avant de s’inscrire !). Ces instances sont fédérées avec les autres (ou pas, selon la décision des administrateurs), ce qui permet aux membres de leurs communautés de pouvoir interagir entre eux.

Prenons par exemple l’instance Mastodon <https://mamot.fr> de [La Quadrature du Net](https://www.laquadrature.net), sur laquelle j’ai ouvert [un compte](https://mamot.fr/ThierryJoffredo) en 2017. Les comptes du fédivers ressemblent à des adresses mail, et sont toujours de la forme nomutilisateur@instance : par exemple, le compte que j’ai ouvert sur [mamot.fr](https://mamot.fr) est ThierryJoffredo@mamot.fr. Vous pouvez consulter la politique de confidentialité de cette instance, elle est disponible [sur cette page](https://mamot.fr/terms)[^5]. Cette instance est fédérée avec de nombreuses autres, ce qui me permet de communiquer et d’interagir avec les membres d’autres instances Mastodon, par exemple [Framapiaf](https://www.framapiaf.org) de [Framasoft](https://framasoft.org/fr/). Je ne peux pas interagir avec des comptes Twitter, en revanche : celui-ci est un service propriétaire et fermé qui n’utilise pas le même protocole de communication. Cela semblerait pourtant naturel : imaginez que vous ouvriez une adresse mail chez un fournisseur qui ne vous autorise à communiquer qu’avec les personnes ayant une adresse mail du même domaine ! C’est pourtant le fonctionnement par défaut, aujourd’hui, de ces réseaux sociaux centralisés, qui empêchent les utilisateurs d’autres services proches d’interagir avec leurs propres utilisateurs.

Avec ce compte Mastodon, ouvert sur l’instance Mamot de La Quadrature du Net, je peux donc m’abonner à des comptes d’autres membres inscrits sur mamot.fr, mais aussi sur d’autres instances fédérées avec Mamot, et suivre leurs publications sur mon fil personnel, interagir (liker, mettre en favori, commenter, partager…) :

{{< figure src="/wp-content/uploads/2022/04/Capture-decran-du-2022-04-24-16-14-00-1024x480.png" caption="Interactions sur Mastodon entre mon compte et les comptes d’autres utilisateurs sur la même instance (Les Jours, le Monde diplomatique) ou sur d’autres (Mediapart, Framasoft)" >}}

Plus fort encore : je peux également interagir avec des instances basées sur d’autres services que Mastodon ! Par exemple, je peux, avec ce même compte actif sur l’instance mamot.fr, m’abonner et interagir avec des comptes sur des services complètement différents de Mastodon, comme Peertube (partage de vidéos) ou PixelFed (partage d’images), comme l’illustration suivante tente d’en rendre compte :

{{< figure src="/wp-content/uploads/2022/04/Capture-decran-du-2022-04-24-16-32-02-1024x426.png" caption="Interactions entre services du fédivers" >}}

Je peux donc visualiser dans mon fil personnel les vidéos publiées sur l’[instance Peertube de La Quadrature du Net](https://video.lqdn.fr/), ou les photographies publiées par un utilisateur ou une utilisatrice de PixelFed – et interagir depuis mon compte Mastodon pour liker, commenter, partager ces publications !

Je peux également choisir d’ouvrir des comptes sur des instances de PixelFed, de Peertube ou d’autres services du fédivers afin d’y publier moi-même des contenus ; par exemple, j’ai récemment ouvert un compte sur l’instance <https://pixelfed.social> pour y publier quelques photographies. Les autres utilisateurs de PixelFed peuvent s’abonner à mon compte s’ils le souhaitent, avec leur propre compte PixelFed (s’ils en ont ouvert un sur une des instances disponibles) ou avec leur compte Mastodon, et interagir avec mes publications. Impossible pour moi, *a contrario*, d’interagir depuis mon compte twitter avec des utilisateurs qui publient des vidéos sur Youtube ou des photos sur Instagram.

## Focus sur un point d’entrée sur le fédivers : Mastodon {#mastodon}

Je suis entré sur le fédivers il y a cinq ans en cherchant une alternative à Twitter. Un service de microblogging proche de Twitter, et qui avait alors rencontré un certain succès, a attiré mon attention : [Mastodon](https://fr.wikipedia.org/wiki/Mastodon_(r%C3%A9seau_social)). J’ai alors ouvert un compte sur l’instance <https://mamot.fr> de La Quadrature du Net, une association que je soutiens pour ses actions et positions en faveur de la défense de nos libertés numériques.

Voici à quoi ressemble l’interface web du service, une fois connecté. Quatre colonnes, la première pour publier ses messages et afficher des résultats de recherche, la seconde pour afficher son fil personnel, la troisième pour les notifications, et la dernière qui peut afficher plusieurs choses – notamment le fil local de l’instance (« local timeline ») et le fil global des instances fédérées avec la mienne (« federated timeline »).

{{< figure src="/wp-content/uploads/2022/04/Mastodon_desktop_web_screenshot-1024x479.png" caption="Source : [https://commons.wikimedia.org/wiki/File:Mastodon\_desktop\_web\_screenshot.png](https://commons.wikimedia.org/wiki/File:Mastodon_desktop_web_screenshot.png), Mastodon.social, AGPL <http://www.gnu.org/licenses/agpl.html>, via Wikimedia Commons" >}}

On lit ou entend souvent que Mastodon est un « clone » de Twitter, mais les différences sont nombreuses :

- différences structurelles, d’abord, comme on l’a vu (plusieurs instances, fédérées entre elles, avec des règles de fonctionnement propres) ;
- différences fonctionnelles également : 
    - limitation par défaut de la taille des messages à 500 caractères ;
    - fil chronologique, sans publicité ;
    - fil local (instance) et global (instances fédérées) ;
    - moindre mise en valeur de la « popularité » d’un message ;
    - paramétrage de la publicité d’un message (public, non listé dans les fils locaux ou globaux, abonné⋅es seulement, ou direct) ;
    - gestion des contenus sensibles : avertissement (CW = content warning), floutage d’image ;
    - export et import de comptes entre instances ;
    - etc.

{{< figure src="/wp-content/uploads/2022/04/Capture-decran-du-2022-04-24-17-01-17-1024x406.png" caption="Composer un message sur Mastodon" >}}

Vous trouverez plus d’informations sur Mastodon à l’adresse suivante : <https://joinmastodon.org/>. Essayez-le, j’espère que cela sera pour vous aussi la clé d’entrée dans ce fédivers que j’ai tâché de vous présenter dans ce billet, sans m’embarrasser de trop de détails techniques (que je serai bien en peine de vous donner d’ailleurs).

## Et dans l’Éducation nationale ? {#educnat}

Plusieurs instances ont été ouvertes par l’Éducation nationale sur le fédivers. Il existe notamment plusieurs instances Peertube, dont :

- [PeerTube Temporaire pour les personnels du Ministère de l’Éducation Nationale](https://tube-education.beta.education.fr/)
- [Peertube de l’académie de Lyon](https://tube.ac-lyon.fr/) (où l’on retrouvera la playlist des vidéos de la 1re journée du libre éducatif, voir [ce billet](https://www.miscellanea-numerica.fr/2022/04/08/retour-sur-la-premiere-journee-du-libre-educatif/))

Il existe également quelques instances Mastodon ouvertes et administrées par des personnels de l’Éducation nationale ou de la Fonction publique :

- [Mastodon du pôle de compétences logiciels libres de l’Éducation nationale](https://mastodon.eole.education)
- [Mastodon Mutualisation inter-ministérielle (MIM) logiciels libres](https://mastodon.mim-libre.fr)

## Ressources complémentaires {#ressources}

- je vous invite très chaudement à aller visiter la page <https://joinfediverse.org> pour mieux entrer dans le fédivers !
- allez également voir le « librecours » de [Stéphane Croizat](https://framapiaf.org/@stph) et Audrey Guélou, intitulé « [low-technicisation et numérique](https://librecours.net/parcours/upload/lownum/index.html) » – voire vous y inscrire ; il y propose en semaine 1 une riche et complète séquence intitulée « [plongée dans le fediverse](https://librecours.net/module/lownum/fedithese/index.xhtml) » [;](https://librecours.net/module/lownum/fedithese/index.xhtml)
    - vidéo de la séquence sur l’instance Peertube de Picasoft : <https://tube.picasoft.net/w/8b45a0c6-2516-4eda-ac16-386695250467>
- dans [le livre *Déclic*](https://declic-lelivre.com/) de Maxime Guedj et Anne-Sophie Jacques, paru en 2020 chez Les Arènes, et dont je vous recommande vivement la lecture, le « petit manuel d’autodéfense numérique » qui clôt l’ouvrage consacre six pages au fédivers (p. 190-195).
- on trouve une présentation du fédivers et de Mobilizon comme alternative à Facebook dans le film documentaire « Disparaître – sous les radars des algorithmes » disponible sur arte.tv (à partir de la 28e minute) : <https://www.arte.tv/fr/videos/100750-000-F/disparaitre-sous-les-radars-des-algorithmes/> (merci [@framaka](https://mastodon.social/@framaka) de me l’avoir rappelé) ;
- à lire également, ce long article traduit en français par l’équipe Framalang et publié sur le Framablog : [« Le Fediverse et l’avenir des réseaux décentralisés »](https://framablog.org/2021/01/26/le-fediverse-et-lavenir-des-reseaux-decentralises/) ;
- plus récent, cet autre article publié sur le Framablog : <https://framablog.org/2022/04/28/elon-musk-et-twitter-vs-mastodon-et-le-fediverse/> (et une chouette vidéo publiée sur Framatube en prime : <https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD>)
- et pour aller plus loin, pourquoi ne pas regarder cette vidéo de [Stéphane Bortzmeyer](https://www.bortzmeyer.org/), qui présente le fonctionnement du fédivers lors des JRES (Journées réseaux de l’enseignement et de la recherche) qui se sont déroulées à Dijon en 2019 – vous y lirez ou entendrez des choses bien plus précises que dans les lignes précédentes : 
    - vidéo de l’intervention (sur la chaîne Peertube des JRES) : <https://replay.jres.org/videos/watch/c228d833-8e00-445a-b38a-60f216933e93>
    - article associé (pdf) : [https://conf-ng.jres.org/2019/document\_revision\_4949.html?download](https://conf-ng.jres.org/2019/document_revision_4949.html?download)

## Remerciements {#merci}

Merci à [@<span>XavCC</span>](https://todon.eu/@XavCC), [@<span>lienrag</span>](https://mastodon.tedomum.net/@lienrag), [@<span>dubs120</span>](https://framapiaf.org/@dubs120), [@<span>Bristow\_69</span>](https://framapiaf.org/@Bristow_69), [@<span>nschont</span>](https://mastodon.mim-libre.fr/@nschont), [@<span>FF255</span>](https://mastodon.tetaneutral.net/@FF255) et [@<span>geb</span>](https://mamot.fr/@geb) (en espérant n’avoir oublié personne) pour leur relecture et leurs suggestions d’améliorations dont ce billet a très largement bénéficié.

[^1]: Et pourquoi ne pas adopter des pratiques qui visent à un « refroidissement social » ? Je vous conseille de faire un petit tour sur [https://www.socialcooling.com/fr/](https://www.socialcooling.com/fr/). 
[^2]: Recommandation à ce sujet : Le meilleur des mondes, France Culture, *Elon Musk à l’assaut de Twitter : un projet politique ?*, émission du 22 avril 2022, [https://www.franceculture.fr/emissions/le-meilleur-des-mondes/le-meilleur-des-mondes-du-vendredi-22-avril-2022](https://www.franceculture.fr/emissions/le-meilleur-des-mondes/le-meilleur-des-mondes-du-vendredi-22-avril-2022).
[^3]: Voir [https://www.lemonde.fr/idees/article/2017/10/14/comment-internet-a-fait-les-printemps-arabes\_5201063\_3232.html](https://www.lemonde.fr/idees/article/2017/10/14/comment-internet-a-fait-les-printemps-arabes_5201063_3232.html) par exemple. |
[^4]: Erratum : Diaspora\* n’utilise pas (encore) le protocole ActivityPub, contrairement à [Friendica](https://friendi.ca/) ou [Hubzilla](https://hubzilla.org//page/hubzilla/hubzilla-project). Merci à lrde@mastodon.tedomum.net de m’avoir signalé cette erreur.
[^5]: Vous verrez à la toute fin une liste d’instances bloquées sur décision des administrateurs : pas d’interaction possible avec ces communautés. 
