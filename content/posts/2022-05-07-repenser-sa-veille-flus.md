---
title: 'Repenser sa veille : Flus'
date: '2022-05-07T09:50:41+02:00'
author: thierry
layout: post
permalink: /2022/05/07/repenser-sa-veille-flus/
image: /wp-content/uploads/2022/05/logo_flus.png
categories:
    - 'Comme ça vient'
---

Ce matin je me suis ré-inscrit sur <https://app.flus.fr> de [@marien](https://tutut.delire.party/@marien).  
Objectifs : fluidifier ma veille, en réunissant dans une seule application ce que je faisais jusqu’à aujourd’hui avec deux (Tiny Tiny RSS et Wallabag), à savoir consulter mes flux RSS, enregistrer des signets, partager ma veille (une première collection publique ici : <https://app.flus.fr/collections/1732141437309404145>).  
Plus d’infos sur le Framablog : <https://framablog.org/2020/12/18/flus-un-media-social-pour-apaiser-votre-veille-sur-le-web/>  
et sur le carnet du projet : <https://flus.fr/carnet/>  
Je vous fais un retour dans qq semaines (et merci [@marien](https://tutut.delire.party/@marien) !)

**Edit** : près d'un après mes débuts, je continue de faire ma veille avec Flus. L'interface est vraiment simple et agréable, "apaisée" - comme souhaité par Marien et Maïwann dès la conception. Je consulte une à deux fois par semaine les liens issus de mes différents flux RSS suivis dans la partie Journal > Les dernières publications : 

![Capture d'écran de mon journal Flus du 25 avril 2023](/mediatheque/flus_journal.png#center)

À partir de là, je peux lire immédiatement les articles / contenus si j'ai le temps sur le moment ou les conserver dans mes signets pour les lire plus tard. Lorsqu'un contenu me semble particulièrement intéressant, je l'ajoute à une collection thématique, qui peut être privée ou publique, et lui ajouter un ou plusieurs commentaires. C'est ainsi que j'alimente ma [collection publique "Miscellanées numériques"](https://app.flus.fr/collections/1732141437309404145), accessible depuis la page d'accueil de ce blog, afin que toute personne intéressée par ma ligne éditoriale (enjeux sociaux du numérique, éducation au numérique, culture numérique, libertés numériques, sécurité numérique et protection des données,...)  puisse y trouver des liens intéressants à visiter (articles, billets, podcasts, vidéos...). Plus de 300 liens y sont aujourd'hui enregistrés, soit presque un par jour depuis sa création.

![Capture d'écran de ma collection flus Miscellanées numériques](/mediatheque/flus_collection.png#center)

En cliquant sur l'un des items de cette liste : 

![Capture d'écran d'un item de cette collection](/mediatheque/flus_collection_lien_commenté.png#center)

Chaque collection a son propre flux RSS, ce qui permet de suivre les veilles d'autres personnes. Il suffit d'ajouter `/feed.atom.xml` à l'url d'une collection pour obtenir l'adresse du flux RSS associé. Ainsi, celui associé à Miscellanées numériques est [https://app.flus.fr/collections/1732141437309404145/feed.atom.xml](https://app.flus.fr/collections/1732141437309404145/feed.atom.xml). Cela me permet, par exemple, de suivre la collection ["Libertés numériques" de Maïwann](https://app.flus.fr/collections/1674384107897557761) en ajoutant le flux RSS associé à ma propre liste de flux suivis.

Dernier point, toujours grâce aux conseils et instructions de Marien, j'ai réussi à automatiser la publication de chaque nouveau lien ajouté dans ma collection Miscellanées numériques sur Mastodon, grâce à un script que j'exécute depuis Framagit : 

![Capture d'écran d'un pouet Mastodon automatiquement publié après ajout d'un lien à ma collection](/mediatheque/flus_masto.png#center)

Pour plus de détails sur cette dernière fonctionnalité, je vous renvoie à la page suivante ; [https://framagit.org/marienfressinaud/veille2toot/](https://framagit.org/marienfressinaud/veille2toot/).

Par ailleurs, d'autres outils existent, également intéressants : je pense à Shaarli, par exemple, qui motorise la [veille du collectif Resnumerica](https://veille.resnumerica.org/), et qui présente quelques fonctionnalités supplémentaires (ajout de tags, liens épinglés, QR code).
