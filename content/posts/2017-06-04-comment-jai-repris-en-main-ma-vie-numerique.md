---
id: 790
title: 'Comment j’ai repris en main ma vie numérique'
date: '2017-06-04T06:05:37+02:00'
author: thierry
excerpt: "Premières tentatives de déGAFAMisation d'un béotien.\n\t\t\t\t\t\t"
layout: post
guid: 'https://www.joffredolebrun.fr/mathonautes/?p=790'
permalink: /2017/06/04/comment-jai-repris-en-main-ma-vie-numerique/
image: /wp-content/uploads/2017/06/512px-Google_Apple_Facebook_Amazon_and_Microsoft.png
categories:
    - 'Hygiène numérique'
---

J’étais il y a peu de temps encore un utilisateur satisfait des services de Google (moteur de recherche, mail, drive, agenda et contacts, photos, maps etc.) et des réseaux sociaux (très actif sur Twitter, avec un millier d’abonné⋅e⋅s, beaucoup moins sur Facebook), et j’utilisais beaucoup de services périphériques pour faciliter ma vie numérique (comme Feedly et Pocket pour accompagner ma veille infodocumentaire, par exemple). Tout cela était bien pratique, mais il me restait un arrière-goût bizarre que je cherchais à ignorer sans y parvenir totalement. Et puis…

… et puis, j’ai eu plusieurs déclics en début d’année qui m’ont sorti de cette torpeur dans laquelle j’étais plongé. Tout d’abord la découverte de MyActivity qui permet aux usagers de Google de visualiser leur activité et une partie des données qui sont enregistrées (essayez, c’est ici : <https://myactivity.google.com/myactivity>). J’ai alors pris conscience, de manière concrète, de tout ce que savait Google de moi : mes recherches sur leur moteur, mes déplacements, mon historique de vidéos sur YouTube, en plus de mes données les plus personnelles comme mes photos, mes agendas ou mes mails. Parallèlement, Twitter – en plus de proposer une ambiance de plus en plus délétère, laissant place aux trolls les plus immondes tout en suspendant les comptes de personnes qui en étaient victimes – modifiait ses conditions d’utilisation et paramètres de confidentialité. Et puis surtout j’ai eu la chance de suivre un atelier organisé par La Quadrature du Net (animé par Adrienne Charmet) lors d’une CryptoParty organisée à Rennes par les bibliothécaires de l’INSA (voir <https://biblio.insa-rennes.fr/crypto/>) intitulé « Rien à cacher, vraiment ? » qui m’a permis de prendre bien conscience du problème de la possibilité d’une surveillance généralisée. Enfin, mon inscription sur Mastodon, le réseau social libre et décentralisé, sur l’instance [mamot.fr](https://mamot.fr) ouverte par La Quadrature du Net, a achevé de me convaincre de neutraliser complètement mes comptes Twitter, Facebook et Instagram

Bref, j’ai repris ma vie numérique en main et je vous expose, très rapidement, un point d’étape sur ce que j’ai réalisé jusqu’à aujourd’hui pour ce faire, histoire de montrer ce qu’il est possible de faire pour commencer, et que ce n’est pas insurmontable (je précise, je n’ai pas le profil informaticien/développeur et je ne me qualifierai pas de « geek » – si tant est que ce mot galvaudé ait un sens !).

### Ordinateur / navigateur / moteur de recherche

J’avais déjà fait une grande partie du chemin il y a maintenant plus de douze ans, en passant de Windows à une distribution Linux grand public, [Ubuntu](https://www.ubuntu.com/desktop). La migration demande de bien évaluer ses propres besoins, de repérer les logiciels qui vous permettent d’y répondre de la meilleure manière sur Windows, et de vérifier qu’ils sont bien disponibles sur la logithèque Ubuntu. De mon côté, TexMaker pour mes documents LaTeX, le client Zotero pour la gestion de ma bibliographie, LibreOffice pour le peu de bureautique que j’utilise, VLC pour les vidéos, Shotwell pour la gestion de ma photothèque, Evolution ou Thunderbird pour la gestion de mes mails, agendas, contacts, etc.

[![](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/Capture-décran-de-2017-06-04-14-46-50-1024x576.png)](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/Capture-décran-de-2017-06-04-14-46-50.png)Côté navigateur, j’ai supprimé toutes mes données enregistrées sur Chrome et suis revenu à [Firefox](https://www.mozilla.org/fr/firefox/new/), que j’ai trouvé bien plus performant que lorsque je l’avais quitté il y a quelques années. J’ai activé la synchronisation des données, ce qui me permet de retrouver mon environnement de navigation sur les autres postes que je suis amené à utiliser au travail ou en mobilité (grâce à l’application Androïd). J’ai modifié mon moteur de recherche par défaut pour [Qwant](https://www.qwant.com/), même si j’ai longtemps hésité avec [DuckDuckGo](https://duckduckgo.com/), tous les deux moteurs de recherche respectueux de la vie privée de leurs utilisateurs.

### Réseaux sociaux

Un grand bol d’air depuis ma migration sur Mastodon (compte principal : <https://mamot.fr/@ThierryJoffredo>) surtout au moment de la campagne électorale pour les élections présidentielles où l’atmosphère sur les réseaux était irrespirable.

Il y a moins de monde sur Mastodon que sur Twitter pour l’instant, bien sûr, mais des interactions bien plus nombreuses, et un esprit – pour l’instant – très différent, plus apaisé et bienveillant. Faites le saut, rejoignez la pachysphère ! Regardez par ici pour commencer : <http://www.kozlika.org/kozeries/post/2017/04/08/Mastodon-premiers-pas>

### Mail, agenda, contacts, cloud

Pour le mail, j’ai simplement décidé d’acheter un nom de domaine chez [Gandi ](https://www.gandi.net/)et je bénéficie de cinq adresses mail associées. Pour consulter cette nouvelle boîte, soit je passe par le webmail proposé par Gandi, soit j’utilise mon logiciel de courrier (en l’occurrence Evolution). Le changement d’adresse auprès des amis et contacts, ainsi que pour les services en ligne, abonnements de newsletter, etc. prend du temps, il faut faire la transition en douceur. De mon côté, en trois semaines, je ne reçois presque plus rien sur mon Gmail. Côté smartphone, j’ai opté pour l’application K9-mail, qui a été relativement simple à paramétrer.

Pour le cloud, les agendas et les contacts, c’était un peu plus compliqué. Après m’être renseigné, j’ai opté pour un hébergement avec 20 Go de stockage toujours chez Gandi, sur lequel j’ai installé [OwnCloud](https://owncloud.com/), une solution logicielle qui me permet de stocker, synchroniser et partager des documents (en remplacement de Drive), mais qui offre aussi deux modules permettant de gérer des agendas et un annuaire de contacts.

[![](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/owncloud-1024x435.png)](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/owncloud.png)

L’installation et le paramétrage ne sont pas vraiment difficiles, mais cela nécessite tout de même d’être bien accompagné⋅e si vous êtes très éloigné⋅e de la question informatique : il est question de création bases de données, d’accès ftp, etc. Je souligne ici la qualité et la réactivité du support de Gandi : pas une seule de mes questions n’est restée sans réponse plus de 24h, toujours avec le souci de se mettre au niveau de son interlocuteur. J’ai également paramétré les applications mobiles qui me permettent d’accéder à ces services : l’appli Owncloud elle-même (pour les fichiers, les photos), une appli spécifique (DavDroid) pour assurer la synchronisation des données d’agenda et de contacts (j’ai même conservé les applis natives pour l’agenda et les contacts, seule la source de l’information a changé).

### Veille et gestion de favoris

Je suis allé encore un peu plus loin pour deux outils que j’utilisais régulièrement dans le cadre de ma veille info-documentaire personnelle et professionnelle : Feedly (agrégateur de flux RSS, qui me permettait de suivre les nouvelles publications sur un peu plus de 200 sites différents) et Pocket (pour enregistrer et classer des articles à lire plus tard). Pour le premier, j’ai à nouveau installé sur mon hébergement Gandi un outil appelé [Tiny Tiny RSS](https://tt-rss.org/fox/tt-rss/wikis/home) qui me rend très bien le service.

[![](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/ttrss-1024x441.png)](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/ttrss.png)

J’utilise également [Wallabag](https://wallabag.org/fr), alternative à Pocket, en choisissant pour l’instant de ne pas l’héberger moi-même, mais de souscrire à leur offre hébergée (12 € / an) :

### [![](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/wallabag-1024x465.png)](https://www.joffredolebrun.fr/mathonautes/wp-content/uploads/wallabag.png)

### Conclusion

J’espère que ce témoignage vous aura intéressé⋅e, et que j’aurai su vous faire comprendre que ce n’était pas si compliqué que cela de commencer à reprendre sa vie numérique en main. Attention, ce n’est pas une démarche qui s’improvise, ni qui se fait instantanément (j’utilise encore quelques services Google, comme YouTube, Maps, Scholar, etc. pour lesquels je n’ai pas encore d’alternative), mais c’est une démarche d’hygiène numérique qui est importante dans le contexte actuel (que je ne développe pas ici).

### Vous voulez essayer ?

Et bien, lancez-vous, ça demande un peu d’énergie, de temps et d’argent (la partie nom de domaine + hébergement 20 Go chez Gandi devrait me coûter environ 10 € par mois en vitesse de croisière), mais ça vaut le coup. Si vous êtes tenté⋅e par une démarche similaire, n’oubliez pas d’aller jeter un coup d’œil du côté de Framasoft, qui permet d’essayer de nombreux services et outils en ligne (Framadrive, Framanews, Framapiaf, etc.) qui vous permettront de vous dégoogliser à votre tour 🙂 <https://degooglisons-internet.org/>

Un grand merci pour finir, donc, à [Framasoft](https://framasoft.org/) et [la Quadrature du Net](https://www.laquadrature.net/fr/) pour leur action !
