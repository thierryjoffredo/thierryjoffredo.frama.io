---
id: 895
title: "«\_Technologies partout, démocratie nulle part\_» de Yaël Benayoun et Irénée Régnauld"
date: '2020-09-28T13:57:29+02:00'
author: thierry
layout: post
guid: 'https://www.joffredolebrun.fr/wp/?p=895'
permalink: /2020/09/28/note-de-lecture-technologies-partout-democratie-nulle-part-de-yael-benayoun-et-irenee-regnauld/
categories:
    - 'Note de lecture'
---

## L’ouvrage

Cet ouvrage de 240 pages environ, sous-titré *Plaidoyer pour que les choix technologiques deviennent l’affaire de tous*, est publié par Yaël Benayoun et Irénée Régnauld, fondateurs de l’association [Le mouton numérique](https://mouton-numerique.org/les_moutons/), aux éditions FYP et vendu au prix de 20€ (18€ en ligne jusqu’au 7 octobre) :

{{< figure src="/wp-content/uploads/2020/09/techdemo.jpg" caption="[Technologies partout, démocratie nulle part. Plaidoyer pour que les choix technologiques deviennent l’affaire de tous](https://boutique.fypeditions.com/products/echnologies-partout-democratie-nulle-part-plaidoyer-pour-que-les-choix-technologiques-deviennent-l-affaire-de-tous)" >}}

Il est composé de 7 chapitres, intitulés :

1. L’irrésistible idéologie du progrès
2. L’âge des « technoluttes »
3. L’insuffisante réponse éthique
4. La démocratie à l’épreuve de la *smart* et de la *safe-city*
5. Le progrès technique à marche forcée dans les entreprises
6. Reprendre en main notre avenir technologique
7. Cinq inspirations pour changer de progrès

## Ce que j’en ai retenu

Pour le dire rapidement et sans ambage, j’ai beaucoup aimé le livre et ai trouvé sa lecture stimulante et revigorante : le livre est très bien structuré, documenté et sourcé (avec une belle bibliographie en fin d’ouvrage), l’écriture est claire et précise, accessible à toute personne intéressée sans connaissances préalables sur le sujet, et le ton est militant et engagé, tout en restant positif (ce qui n’est pas une mince affaire en ces temps de sinistrose).

Il s’ouvre sur un plaidoyer contre l’inéluctabilité supposée des évolutions techniques et d’une forme de déterminisme technologique (« on n’arrête pas le progrès »), dénonce une polarisation du discours entre technophiles et technophobes (et l’assimilation des voix critiques à celles des ignorants et complotistes de tout poil), et s’élève contre l’érection de l’innovation en « principe hiérarchiquement supérieur à tous les autres » – tout en soulignant au passage un glissement sémantique intéressant entre le mot « progrès », porteur d’une dimension sociale, qui n’est plus guère utilisé, et le mot « innovation » qui renvoie davantage aux dimensions utilitaristes et économiques de la technologie.

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-5G-a-Rennes-Les-patrons-du-numerique-abasourdis-par-la-decision-de-la-mairie-1024x433.png" caption="« Abasourdis » ! [Ouest-France](https://www.ouest-france.fr/bretagne/rennes-35000/5g-a-rennes-les-patrons-du-numerique-abasourdis-par-la-decision-de-la-mairie-6978019)" >}}

Après avoir rappelé les quelques (maigres) victoires des technoluttes récentes, les auteurs nous renvoient à l’insuffisance des réponses aux questionnements éthiques qui devraient accompagner tout déploiement d’une technologie à l’échelle d’une organisation, d’une ville, d’un État : notre incapacité à tracer des lignes rouges infranchissables en faveur de la protection des données personnelles, des libertés individuelles et de l’environnement (au profit de réductions de biais qui ne changent rien au fond du problème), à éviter l’invisibilisation des enjeux et la dilution des responsabilités (avec l’ingénieur⋅e qui est censé⋅e la porter en dernier ressort, ou jouer le rôle de lanceur d’alerte !), ou à lutter contre les connivences entre pouvoirs politiques et puissances économiques qui s’exercent au détriment des débats publics et des régulations. Là où nous nous contentons presque toujours de chartes d’usage, de bonnes pratiques, de déclarations d’intention ou de comitologies non coercitives, il faudrait repolitiser les débats et relever très nettement le niveau d’exigence de nos démarches éthiques en prenant des positions claires, éclairées, démocratiques et soucieuses du bien commun, s’imposant aux acteurs publics et industriels ou marchands.

Le chapitre sur les *smart* et *safe-cities* permet de bien comprendre, au travers de quelques exemples bien choisis – comme celui du projet Sidewalk à Toronto mené par la société mère de Google, Alphabet, récemment abandonné – les enjeux du déploiement dans les villes de technologies permettant de vastes collectes de données et d’inquiétantes opérations de surveillance sous couvert de souci écologique ou sécuritaire.

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-A-Toronto-Bianca-Wylie-defie-Google-et-sa-ville-connectee-1024x740.png" caption="[Le Monde](https://www.lemonde.fr/idees/article/2019/12/23/a-toronto-bianca-wylie-defie-google-et-sa-ville-connectee_6023873_3232.html) (abonné⋅es)" >}}

Le cas de la reconnaissance faciale, expérimentée en France dans des lycées niçois par exemple, et banalisée – rendue acceptable – par des usages triviaux de pseudo-sécurisation de certains smartphones, est l’exemple même d’une technologie liberticide qu’il convient d’interdire purement et simplement, comme quelques villes américaines l’ont fait récemment. Aujourd’hui, en France, seules quelques associations (comme La Quadrature du Net, avec son projet Technopolice) permettent de maintenir une certaine pression sur la décision politique au prix de procédures et de recours, mais leurs efforts seront-ils suffisants ?

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-Bannissons-la-reconnaissance-faciale-en-Europe--1024x382.png" caption="[Pétition en ligne](https://ban-facial-recognition.wesign.it/droitshumains/bannissons-la-reconnaissance-faciale-en-europe) contre l’utilisation de la reconnaissance faciale en Europe" >}}

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-Technopolice-1024x496.png" caption="[Technopolice](https://technopolice.fr/)" >}}

Le chapitre suivant aborde la question de l’automatisation de certains métiers et, dans un même mouvement, de l’invisibilisation et de la dégradation du travail réel qui sous-tend cette automatisation. L’exemple de l’automatisation des caisses de supermarché est à ce titre édifiante : présentée comme un moyen de libérer l’hôte⋅sse de caisse de ses tâches les moins intéressantes afin de mieux servir les client⋅es, les auteurs montrent qu’elle empêche « paradoxalement » les salarié⋅es occupant ces postes de faire correctement leur métier en les éloignant de la clientèle, tout en n’apportant aucun confort supplémentaire à cette clientèle, bien au contraire. L’affaiblissement des instances de dialogue et de négociation en entreprise et des organisations représentatives des salariés, la surindividualisation du travail (favorisée et encouragée par de nouvelles formes de management et les technologies qui les outillent), empêchent l’émergence d’expressions et de revendications collectives dans les entreprises, renvoyant l’employé à sa propre responsabilité (phénomène complètement exacerbé chez les travailleurs du clics sur les plateformes de micro-travail !).

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-Invisibles-1024x576.png" caption="Indispensable série documentaire d’Arte sur les travailleurs du clic, avec Antonio Casilli : <https://www.france.tv/slash/invisibles/" >}}

Alors que faire, face à ces constats amers ? Les deux derniers chapitres redonnent quelque espoir en donnant des perspectives sur nos moyens d’action, qui tiennent dans les trois intertitres du chapitre :

- fixer des limites au développement technologique avec un cadre institutionnel et légal fort (ex : RGPD), des lignes rouges clairement et collectivement établies, en nous accordant sur nos valeurs communes et le sens donné à nos activités ;
- renforcer les contre-pouvoirs démocratiques par des dispositifs de participation citoyenne / de démocratie technique, à l’illustration de la Convention citoyenne pour le climat – avec le danger de voir leurs délibérations ignorées ! -, agir à l’échelle des villes (municipalisme), proposer d’autres modèles d’organisation collective (coopératives), et ne pas laisser collectifs et associations de défense des libertés seules face aux puissances publiques et économiques ;
- remettre l’économie à sa juste place, au travers de ces contre-pouvoirs démocratiques, en la mettant au service des citoyen⋅nes et en l’assujettissant aux enjeux sociaux et environnementaux actuels.

Les cinq « inspirations » qui concluent l’ouvrage (le chapitre est introduit par une mention prémonitoire aux Amish !) viennent illustrer par l’exemple des moyens de réappropriation démocratique des technologies, par l’éducation, la recherche d’alternatives *low-tech*, les expériences de démocratie technique à l’étranger, le municipalisme libertaire ou l’approche des coopératives comme alternatives aux plateformes.

À voir, sur la question de l’exercice réel de la démocratie pour le peuple, par le peuple : 

{{< peertube peertube.datagueule.tv 0b04f13d-1e18-4f1d-814e-4979aa7c9c44 >}}

À lire également, cet [entretien de Christophe Masutti](https://lvsl.fr/aux-sources-du-capitalisme-de-surveillance-entretien-avec-christophe-masutti/) pour Le vent se lève, sur le capitalisme de surveillance et les approches possibles de lutte et d’opposition.

## Et maintenant ?

Il me reste bien entendu à transposer les réflexions provoquées par la lecture de l’ouvrage au contexte particulier du numérique éducatif. Quel positivisme se cache derrière les déploiements matériels et logiciels dans écoles et les établissements, quels sont les impacts sur le plan éducatif, professionnel, mais aussi social et écologique des environnements numériques que l’on déploie ? Toute « innovation » en la matière représente-t-elle un progrès, et est-elle souhaitable, comment peut-elle être encadrée, quels sont nos garde-fous ? Comment la communauté éducative et, au-delà, les citoyens (nous sommes tous touché⋅es par l’école de près ou de loin) peut-elle parvenir à se saisir de ces questions et proposer des formes d’instances démocratiques guidant les choix relatifs au numérique à l’école : matériel (en favorisant les réemplois), offre logicielle (en privilégiant voire en imposant les logiciels libres), protection des données scolaires (en imposant des cadres stricts aux acteurs du secteur marchand et de la filière EdTech), production et partage de ressources pédagogiques sur le modèle des communs, co-éducation et participation des familles (démarches en ligne ou guichet d’accueil dans les établissements), etc. ?

Les chantiers sont nombreux. Espérons que les États généraux du numérique, prévus cet automne et souhaités par le ministre de l’Éducation pour faire écho aux prises de conscience provoquées par le confinement, puissent sinon apporter des réponses immédiates, au moins permettre l’expression et la rencontre d’idées qui aillent dans le sens de plus de démocratie dans la prise de décision vis-à-vis de technologies pour l’éducation. Les contributions sur la thématique « Un numérique responsable et souverain » sont ouvertes à toutes et tous, n’hésitez pas à proposer les vôtres sur la plateforme participative mise à disposition à cet effet (motorisée par la plateforme libre *[Decidim](https://labo.societenumerique.gouv.fr/2019/09/11/decidim-un-commun-numerique-pour-la-participation-citoyenne/)* utilisée à Barcelone !) :

{{< figure src="/wp-content/uploads/2020/09/Screenshot_2020-09-28-Un-numerique-responsable-et-souverain-Etats-generaux-du-numerique-pour-leducation-1024x712.png" caption="[Etats généraux du numérique 2020](https://etats-generaux-du-numerique.education.gouv.fr/processes/numerique-souverain/)" >}}

Faites vos propositions sur <https://etats-generaux-du-numerique.education.gouv.fr/processes/numerique-souverain/>.

Reste à espérer que ces États généraux soient le point de départ d’une réflexion profonde et durable sur les enjeux, objectifs et moyens du service public du numérique éducatif (entre État, collectivités, opérateurs…) et ne feront pas que participer à « créer de l’acceptabilité » pour la mise en œuvre de politiques ou de solutions qui s’imposeraient sans réelle concertation démocratique au sens délimité par cet excellent ouvrage, dont je vous conseille vivement la lecture, donc.
