---
id: 1047
title: 'Comment j’ai repris en main ma vie numérique (suite)'
date: '2020-12-28T17:55:02+01:00'
author: thierry
layout: post
guid: 'https://www.miscellanea-numerica.fr/?p=1047'
permalink: /2020/12/28/comment-jai-repris-en-main-ma-vie-numerique-2/
image: /wp-content/uploads/2017/06/512px-Google_Apple_Facebook_Amazon_and_Microsoft.png
categories:
    - 'Hygiène numérique'
---

Dans un [premier billet](/2017-06-04-comment-jai-repris-en-main-ma-vie-numerique) j’expliquais ce qui m’avait mené à me « da-gafamiser » il y a quelques années, ce qui s’était traduit par l’usage systématique, dans le cadre professionnel comme personnel, de logiciels et services libres et protecteurs de ma vie privée (système d’exploitation, navigateur, moteur de recherche, mail, bureautique, etc.) et par l’ouverture d’un hébergement chez Gandi pour abriter des services numériques personnels (cloud, agenda, agrégateur de flux RSS…).

J’aimerais aujourd’hui compléter par ce que j’ai fait ces derniers mois pour reprendre le contrôle de mon téléphone portable (un Sony XPeria sous Android 6.0), bien plus sujet à la surveillance et aux traçages / pistages de toutes sortes et plus difficile à protéger que mes ordinateurs.

## Réglage des paramètres de confidentialité

La sécurité de son téléphone portable est égale à celle de son application la moins sécurisée. Il faut donc systématiquement penser à minimiser les permissions, n’activer la géolocalisation que lorsque cela vous est nécessaire, désactiver la personnalisation des annonces publicitaires, et bien penser à pousser au maximum les paramètres de confidentialité de vos applications. Voir quelques instructions [sur le site de la CNIL](https://www.cnil.fr/fr/maitrisez-les-reglages-vie-privee-de-votre-smartphone) par exemple.

Si vous avez un téléphone sous Android, il vous faut absolument analyser les permissions accordées à vos applications et visualiser les pisteurs embarqués grâce à [Exodus Privacy](https://exodus-privacy.eu.org/fr/), indispensable outil qui va vous permettre de scanner votre téléphone et de produire un rapport sur les applications installées. Un exemple parlant de rapport produit par Exodus Privacy sur l’application Marmiton :

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-28-εxodus.png" >}}

Pour en savoir plus sur Exodus Privacy, je vous recommande [cette émission](https://www.franceinter.fr/emissions/le-code-a-change/ils-cherchent-les-trucs-bizarres-qu-il-y-a-dans-vos-telephones-rencontre-avec-des-traqueurs-de-trackers) du podcast Le code a changé.

## F-Droid, un magasin d’applications alternatif à Google Play

Celui-ci n’est pas encore assez connu, et c’est bien dommage : [F-Droid](https://f-droid.org/fr/) est un magasin d’applications libres et open source, respectueuses de la vie privée des utilisateurs, qui gagnerait à être mieux mis en lumière, car on y trouve d’excellentes alternatives aux applications du Play Store.

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-28-F-Droid-Free-and-Open-Source-Android-App-Repository-1024x794.png" link="https://f-droid.org/fr/" >}}

C’est là que je vais chercher plusieurs des applications que j’utilise quotidiennement, avec beaucoup de sécurité mais aussi de confort :

- AntennaPod, pour gérer et écouter mes podcasts ;
- Blokada, pour protéger ma navigation des publicités et des pisteurs ;
- DAVx5, pour synchroniser mes agendas ;
- Etar, pour présenter et gérer mes agendas ;
- Exodus Privacy, pour connaître les pisteurs et permissions des applications installées sur mon téléphone ;
- F-Droid, pour installer et mettre à jour les applications ;
- FairEmail, pour gérer mes quatre adresses de courrier électronique ;
- Fedilab, pour accéder à mon flux et mes listes Mastodon ;
- Jitsi Meet, pour mes webconférences lorsque je ne peux pas être devant mon poste de travail ;
- MuPDF, pour lire les documents au format PDF ;
- NewPipe, pour accéder aux vidéos de mes abonnements YouTube (sans être tracé) et PeerTube ;
- NextCloud, pour accéder à mon instance hébergée chez Gandi ;
- Obsqr, pour lire les QR-codes ;
- OsmAnd, OpenStreetMap pour Android ;
- Pocket Paint, pour éditer des images ;
- Rocket.Chat, pour accéder à ma messagerie pro ;
- Simple File Manager
- Simple Gallery Pro, pour afficher mes photos ;
- TinyTinyRSS, agrégateur de flux RSS ;
- Wallabag, pour sauvegarder et lire mes articles plus tard ;
- Wikipedia, pour accéder aux contenus de l’encyclopédie en ligne.

Toutes mes applications ne sont pas disponibles sur F-Droid ; je dois encore utiliser le Play Store de Google pour les applications suivantes, également libres et open source :

- Firefox pour Android, pour ma navigation sur le web, avec ses extensions incontournables : uBlock Origin, Privacy Badger (\[edit\] sur Mastodon on me signale le projet [Fennec](https://f-droid.org/fr/packages/org.mozilla.fennec_fdroid/)) ;
- Cozy Drive, pour accéder à mes fichiers hébergés chez Cozy ;
- Cozy Pass, mon gestionnaire de mots de passe ;
- Collabora Office (LibreOffice) pour éditer des documents bureautiques (\[edit\] toujours sur Mastodon on me signale [ce dépôt](https://www.collaboraoffice.com/downloads/fdroid/repo/) à ajouter à F-Droid pour en bénéficier) ;
- Magic Earth (pour tenter de remplacer Google Maps, en alternative à OsmAnd) ;
- Signal, pour ma messagerie perso ;

et toutes les applications non libres dont j’ai tout de même régulièrement besoin : banque, cinéma, opérateurs, transports en commun, réseaux sociaux (Twitter, Instagram), YouTube Music (où j’ai toujours ma bibliothèque musicale personnelle, n’ayant pas trouvé d’autre service pour l’héberger pour l’instant) et quelques autres (Gallica, carte du ciel, etc.).

On peut noter ici que je me passe de plusieurs applications issues du Play Store en utilisant, lorsqu’elles sont disponibles, des PWA (pour Progressive Web Apps, voir [cet excellent article](https://avant-dane.blog.ac-lyon.fr/2019/12/16/arretons-dinstaller-des-apps-utilisons-les-pwa/) publié par @Bristow\_69 sur le site de la DANE de Lyon l’an passé) comme pour Le Monde, [NextInpact](https://www.nextinpact.com/article/43391/la-revolution-applications-web-progressives-pwa), Instagram, Twitter ou Pronote par exemple, ce qui offre plusieurs avantages – indépendance vis-à-vis de la plateforme, légèreté, mises à jour en arrière-plan, fluidité et rapidité – mais pas nécessairement sur la protection de la vie privée, néanmoins (même si, avec ces PWA basées sur un navigateur respectueux de la vie privée comme Firefox et ses extensions, les pisteurs sont majoritairement bloqués !).

{{< figure src="/wp-content/uploads/2020/12/Screenshot_20201230-112112-1-576x1024.png" caption="Pour installer une PWA (ici, lemonde.fr) depuis le menu de Firefox pour Android." >}}

## Se débarrasser (enfin !) des applications GAFAM et constructeur non désinstallables

Grâce à [@Bristow\_69](https://mamot.fr/web/statuses/105407126847576271) et [@alainmi11](https://mamot.fr/web/statuses/105411649903273313), qui ont récemment remis en lumière une procédure de désinstallation de ces applications installées par défaut et seulement désactivables (applications Google, Facebook et constructeur – Sony, en l’espèce), j’ai pu alléger mon téléphone de quelque 23 applications inutiles que j’avais désactivées mais qu’il m’était impossible de désinstaller.

La procédure est assez simple pour celle ou celui qui possède un ordinateur sous Linux et ne craint pas de saisir quelques lignes de commande (détaillée [ici](https://www.dadall.info/article657/nettoyer-android), par exemple). Pour ma part – sous Ubuntu 20.04 – ça s’est limité à connecter mon téléphone en USB à mon ordinateur, activer le mode développeur sur le téléphone et à lancer les commandes suivantes dans un terminal sur mon PC :

- l’installation du paquet android-tools-adb :

sudo apt install android-tools-adb

- la suppression, une à une, des applications système :

adb shell pm uninstall –user 0 com.facebook.system

## Et maintenant ?

Ce n’est pas terminé, j’ai encore quelques applications dont je ne me suis pas encore parvenu à me délester, pour des raisons diverses et variées :

- Google Livres
- Google Maps (décidément difficile à remplacer)
- YouTube Music (\[edit\] alternative dans l’univers Nextcloud proposée par @Bristow\_69 : <https://rello.github.io/audioplayer/>)
- Whatsapp (messagerie famille)
- <s>Instagram</s> (disponible en PWA, sans multi-comptes)
- Twitter (disponible en PWA, mais il me manque une fonctionnalité très utile pour l’instant : les listes épinglées en onglet)

Ça viendra avec le temps. L’étape suivante est sans doute de basculer sur un téléphone libéré du système d’exploitation de Google ; je pense, au prochain renouvellement d’appareil (mon Sony XPeria marche encore bien après 4 ans), me diriger vers un [Fairphone](https://shop.fairphone.com/fr/fairphone3) équipé du [système /e/](https://e.foundation/fr/) et donc nativement débarrassé des applications des GAFAM.

En complément de ce billet, deux ou trois podcasts qui parlent des problèmes causés par la fabrication et les usages des smartphones, avec notamment Nicolas Nova, auteur de [Smartphones. Une enquête anthropologique](https://www.metispresses.ch/fr/smartphones) (Métis Presses, 2020) :

- sur ARTE\_Radio : [https://www.arteradio.com/son/61665033/comment\_dresser\_son\_smartphone](https://www.arteradio.com/son/61665033/comment_dresser_son_smartphone)
- sur France Culture : <https://www.franceculture.fr/emissions/la-suite-dans-les-idees/intelligence-du-smartphone>
- Au sources du numérique (ASDN) : [https://youtu.be/x-Rx-P\_75UM](https://youtu.be/x-Rx-P_75UM)

\[Edit\] Le meilleur moyen de reprendre en main sa vie numérique est sans doute d’éteindre bien plus souvent son téléphone portable 😉 .

Crédit illustration : Knolskape, Public domain, via [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Google,_Apple,_Facebook,_Amazon_and_Microsoft.png).
