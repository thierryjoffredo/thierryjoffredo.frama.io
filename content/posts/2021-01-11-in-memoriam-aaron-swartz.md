---
id: 1191
title: 'In memoriam Aaron Swartz'
date: '2021-01-11T14:13:33+01:00'
author: thierry
layout: post
guid: 'https://www.miscellanea-numerica.fr/?p=1191'
permalink: /2021/01/11/in-memoriam-aaron-swartz/
categories:
    - 'Libertés numériques'
---

{{< figure src="/wp-content/uploads/2021/01/Aaron_Swartz_2_at_Boston_Wikipedia_Meetup_2009-08-18.jpeg" width="400" >}}

Le 11 janvier 2013, Aaron Swartz, fervent acteur et défenseur des libertés numériques, se suicidait dans son appartement new-yorkais, quelques jours avant le début de son procès fédéral, accusé de fraude électronique pour le téléchargement du catalogue JSTOR.

[https://fr.wikipedia.org/wiki/Aaron\_Swartz](https://fr.wikipedia.org/wiki/Aaron_Swartz)

C’est une figure inspirante qui m’a beaucoup marqué. Pour re-découvrir ce qu’il a apporté : contributions techniques aux spécifications du [RSS](https://web.resource.org/rss/1.0/), définition des licences [Creative commons](https://creativecommons.org/licenses/?lang=fr-FR), l’[Open access](https://framablog.org/2013/01/14/manifeste-guerilla-libre-acces-aaron-swartz/), les [moyens d’action](https://demandprogress.org/) mis à disposition des activistes, on peut lire le formidable livre-enquête de Flore Vasseur, intitulé « Ce qu’il reste de nos rêves » : <https://florevasseur.com/books/ce-quil-reste-de-nos-reves/>

{{< figure src="/wp-content/uploads/2021/01/COVER-Ce-quil-reste-de-nos-reves-HD-e1544539314223.jpeg" >}}

On peut également voir « The Internet’s own boy », le film-testament que lui a consacré Brian Knappenberger en 2014 :

{{< peertube video.antopie.org a3352705-02ff-4c25-bd95-29462d28e92b >}}

<figure class="wp-block-embed is-type-wp-embed is-provider-peertube wp-block-embed-peertube"><div class="wp-block-embed__wrapper"><iframe class="wp-embedded-content" data-secret="8eT5SJEzLJ" frameborder="0" height="315" sandbox="allow-scripts" security="restricted" src="https://video.antopie.org/videos/embed/a3352705-02ff-4c25-bd95-29462d28e92b#?secret=8eT5SJEzLJ" title="The Internet's own boy - The story of Aaron Swartz " width="500"></iframe></div></figure>

Enfin, on pourra lire le tout récent billet intitulé [Aaron Swartz : sa vie, ses combats](https://cultinfo.hypotheses.org/599) d’Anne Cordier et Sophie Bocquet sur le blog Hypotheses *Cultures de l’information,* publié dans le cadre de leur projet « Figures en commun(s) ».
