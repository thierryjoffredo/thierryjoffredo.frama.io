---
id: 939
title: 'À propos d’une tribune parue dans Reporterre'
date: '2020-12-15T13:53:00+01:00'
author: thierry
layout: post
guid: 'https://www.joffredolebrun.fr/wp/?p=939'
permalink: /2020/12/15/a-propos-dune-tribune-parue-dans-reporterre/
categories:
    - 'Numérique pour l''éducation'
---

{{< figure src="/wp-content/uploads/2020/12/15712427278_a41187398d_c.jpg" caption="Zach Frailey, [Creative Commons BY-NC-ND 2.0](https://creativecommons.org/licenses/by-nc-nd/2.0/) sur Flickr : <https://flic.kr/p/pWsidA>" >}}

Le 12 décembre une tribune intitulée « La numérisation de l’école est nuisible aux enfants et à l’environnement » a été publiée sur le site Reporterre.net (le quotidien de l’écologie). Vous pouvez lire cette tribune à l’adresse suivante :

<https://reporterre.net/La-numerisation-de-l-ecole-est-nuisible-aux-enfants-et-a-l-environnement>

Elle a été initiée par les collectifs [Lève les yeux !](https://www.levelesyeux.com/) (collectif pour la reconquête de l’attention), [Nous personne](http://nouspersonne.fr/), et [Cose](http://www.surexpositionecrans.org/) (collectif surexposition écrans). Je vous laisse vous faire votre propre idée sur les initiateurs de la tribune et leurs positions, ce n’est pas l’objet de ce billet. Elle est co-signée par une quinzaine d’associations dont [GreenIT](https://www.greenit.fr/) et [HOP](https://www.halteobsolescence.org/) (Halte à l’obsolescence programmée), que je suis avec intérêt, pour ma part. C’est d’ailleurs par un message de HOP sur Twitter que j’ai pris connaissance de cette tribune ce matin :

{{< tweet user="HalteOP" id="1338720451846152193" >}}

La lecture de la tribune m’a fait réagir, et je reconstitue / complète un peu [l’enfilade de mes tweets](https://twitter.com/ThierryJoffredo/status/1338725224444030979?s=20) dans ce billet.

Je suis bien évidemment très favorable à une approche techno-critique du numérique dans l’éducation, mais pas pour écrire n’importe quoi. Les impératifs économiques, écologiques, démocratiques actuels doivent nous guider aujourd’hui dans nos choix pour l’éducation, et il est urgent de (re)faire de la place à l’humain et aux communs plutôt qu’à la technologie et au marchand. Là-dessus, je marque mon accord avec le fond du texte. Néanmoins, pour le reste, je déplore totalement l’argumentation développée dans cette tribune. Développons un peu :

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-15-La-numerisation-de-lecole-est-nuisible-aux-enfants-et-a-lenvironnement-1024x885.png" width="400" >}}

1- si l’exploitation de l’enquête PISA montre des corrélations (et pas des causalités) légèrement négatives, l’analyse de l’OCDE montre un peu plus de complexité que ce qui est affirmé ici ([https://dx.doi.org/10.1787/9789264239555-en](https://t.co/ukR0jm3xNY?amp=1), ch. 6 p. 145-164). Je vous conseille la lecture du [rapport du CNESCO](http://www.cnesco.fr/fr/numerique-et-apprentissages-scolaires/) sur le numérique et les apprentissages scolaires, daté d’octobre dernier, pour aborder cette question complexe, largement étudiée sous plusieurs angles scientifiques mais non clairement tranchée aujourd’hui.

2- et personne n’a jamais prétendu que « l’écran » enseignait mieux que l’humain !

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-15-La-numerisation-de-lecole-est-nuisible-aux-enfants-et-a-lenvironnement1-847x1024.png"  width="400" >}}

3- sur le mythe des parents de la Silicon Valley (3 millions d’habitants) et cette fameuse école Waldorf-Steiner (200 élèves), les articles fantaisistes se sont multipliés en 2013, et ont fait florès, faisant de cette « information » un lieu commun dont il est bien difficile de se débarrasser, visant à faire croire (aux limites du complotisme) que les cadres du numérique à San Francisco protègent leurs enfants d’une éducation accompagnée par le numérique tout en y exposant sans vergogne le reste de la population mondiale ; voir [https://liberation.fr/checknews/2017/11/07/est-il-vrai-que-les-cadres-de-la-silicon-valley-envoient-leurs-enfants-dans-des-ecoles-sans-nouvelle\_1652713](https://t.co/kxGZcsAvW6?amp=1)

4- ces 45 états US (comme la Finlande, par exemple) n’ont fait que sortir l’écriture **cursive** des attendus des programmes scolaires en 2013 (repris en 2016). ceci a donné lieu, là aussi, à des quantités invraisemblables d’articles sur le déclin de l’écriture et l’abrutissement corollaire des masses. Sachez qu’on y apprend donc toujours à écrire à la main (écriture en script, et au clavier également), l’écriture cursive étant néanmoins toujours considérée comme très importante pour l’apprentissage ; voir [https://vousnousils.fr/2017/08/01/aux-usa-lecriture-cursive-fait-son-come-back-a-lecole-606046](https://t.co/wqo3en0TPd?amp=1) par exemple.

{{< figure src="/wp-content/uploads/2020/12/Screenshot_2020-12-15-La-numerisation-de-lecole-est-nuisible-aux-enfants-et-a-lenvironnement2-1-1024x453.png" width="400" >}}

5- quant à la question sanitaire de l’exposition aux ondes, pas de lien vers une référence scientifique, qui serait pourtant bienvenue si elle pouvait être étayée.

En revanche, d’accord pour plus de démocratie dans les choix qui président aux orientations de l’école en général et au numérique éducatif en particulier ; d’accord pour une évaluation impartiale, par la recherche, des bénéfices et des inconvénients du numérique pour l’éducation ; d’accord pour une sobriété numérique qui tienne compte des enjeux économiques, écologiques, éducatifs et sanitaires de manière partagée et éclairée ; et d’accord pour plus d’humain – et donc plus de moyens en enseignants, personnels de santé, d’orientation, d’accompagnement – dans les relations entre les membres de la communauté éducative (services, écoles et établissements, élèves et familles), sans médiation contrainte du numérique ; d’accord pour un numérique qui émancipe et libère plutôt que contrôle et aliène. Mais ce n’est pas en publiant des tribunes intellectuellement malhonnêtes qu’on va faire avancer les choses, et la techno-critique ne se satisfait pas de quelques références glanées sur les internets sans vérification, réflexion ou analyse.

Une autre tribune critique sur le sujet du numérique éducatif publiée sur Reporterre, bien mieux documentée et étayée, signée Christophe Cailleaux, Amélie Harte-Hutasse et François Jarrige, est à lire à cette adresse : <https://reporterre.net/L-ecole-confinee-laboratoire-du-monde-numerique>
.
