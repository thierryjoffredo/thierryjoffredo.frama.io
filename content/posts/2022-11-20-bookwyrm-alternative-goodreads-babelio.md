---
title: 'Bookwyrm, une alternative (libre et décentralisée) à Goodreads et Babelio'
date: '2022-11-20T16:09:35+01:00'
layout: post
permalink: /2022/11/20/bookwyrm-alternative-goodreads-babelio/
categories:
    - 'Comme ça vient'
---

Vous aimez partager et commenter vos lectures ? Chercher l’inspiration pour votre prochaine lecture auprès de gens dont vous partagez les goûts ? Entamer une conversation avec d’autres lecteurs ou lectrices autour des livres que vous avez lus ? Peut-être le faites-vous déjà sur Babelio ou Goodreads, mais souhaitez le faire sur une plateforme qui respecte et protège vos données personnelles et votre vie privée (car oui, vos lectures disent beaucoup de vous-mêmes) ? Alors Bookwyrm est une alternative intéressante à considérer !

{{< figure align=center src="/wp-content/uploads/2022/11/bookwyrm_logo.png" caption="Logo de Bookwyrm" >}}

Bookwyrm, comme par exemple [Mastodon, le service de micro-blogging dont on entend beaucoup parler en ce moment](https://www.lemonde.fr/pixels/article/2022/11/09/comment-fonctionne-mastodon-presente-comme-alternative-libre-a-twitter_6149183_4408996.html)), est un des nombreux services libres et décentralisés du [fédivers](/posts/2022-04-24-petite-introduction-au-fedivers/) (une large fédération de serveurs formant un réseau social, communiquant entre eux grâce à un protocole de communication commun, au travers de divers services comme Mastodon pour le micro-blogging, PixelFed pour le partage d’images et de photographies, Peertube pour les vidéos, etc., eux-mêmes porteurs de nombreuses alternatives aux plateformes propriétaires). Il faut donc commencer par choisir le serveur (on dit « ‘linstance ») sur lequel on va s’inscrire, qui sera votre petite communauté de confiance, interconnectée aux autres instances pour que vous puissiez interagir avec leurs utilisateurs (s’abonner, commenter les contributions, etc.). Pour faire ce choix – et découvrir Bookwyrm – vous pouvez commencer par visiter le site <https://joinbookwyrm.com/fr/>, ce qui vous permettra, entre autres, de choisir votre serveur sur les critères qui vous appartiennent : la langue utilisée par les utilisateurs de l’instance peut être un élément de choix important, par exemple.

Pour ma part, j’ai choisi une petite instance francophone qui, à l’heure où j’écris, accepte toujours les inscriptions, réunit quelques dizaines de personnes, et que vous pouvez découvrir ici : <https://bw.diaspodon.fr/discover>. Mais attention, l’administrateur de l’instance indique qu’il a ouvert cette instance pour tester le service, qu’il ne souhaite pas qu’elle soit référencée sur des pages comme joinbookwyrm, par exemple, de demande de préciser que « c’est vraiment de la peinture fraîche et que ça peut disparaître du jour au lendemain si le logiciel ne tient pas la route ».

{{< figure src="/wp-content/uploads/2022/11/bookmyrm_explore-1024x793-1.png" >}}

Une fois inscrit⋅e, vous pouvez donc :

- compléter votre bio pour vous présenter aux autres membres de la communauté ;
- vous abonner à d’autres comptes, de votre instance ou d’autres instances (par exemple, de <https://bookwyrm.social>, qui regroupe la plus grosse communauté de lecteurs et lectrices – près de 4500 – mais attention, *small is beautiful*, espérons que d’autres instances, notamment francophones, ouvrent dans les semaines et les mois à venir) ;
- suivre un fil d’actualités qui vous informe des lectures de vos abonnements et des critiques publiées ;
- liker, commenter les critiques déjà publiées, et entamer ainsi une conversation ;
- noter vous-mêmes les ouvrages référencés, et écrire vos premières critiques (en important un livre s’il n’est pas déjà référencé sur la base).

{{< figure src="/wp-content/uploads/2022/11/Screenshot-2022-11-13-at-07-47-53-BookWyrm-1024x957-1.png" >}}

Une remarque importante pour vous lancer : les personnes ayant déjà un historique sur Goodreads peuvent importer leurs livres et leurs critiques, ce qui leur permet de ne pas partir de zéro :

{{< figure src="/wp-content/uploads/2022/11/Screenshot-2022-11-13-at-07-44-11-BookWyrm.png" >}}

Ce billet a été initialement publié (par moi-même) sur le site [https://alternatives-numeriques.fr](https://alternatives-numeriques.fr/bookwyrm-une-alternative-libre-et-decentralisee-a-goodreads-et-babelio/) sous licence [Creative Commons BY SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
